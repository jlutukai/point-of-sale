package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.Shift
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.android.synthetic.main.item_shift.view.*

class ShiftsAdapter (private val context: Context, private val shifts: List<Shift>) :
    RecyclerView.Adapter<ShiftsAdapter.ShiftsViewHolder>(){
    var onItemClick: ((Shift) -> Unit)? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShiftsViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_shift, parent, false)
        return ShiftsViewHolder(view)
    }

    override fun getItemCount(): Int = shifts.size

    override fun onBindViewHolder(holder: ShiftsViewHolder, position: Int) {
        val shift = shifts[position]
        holder.setData(shift, position)
    }

    inner class ShiftsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Shift? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper_shift.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }
        fun setData(shift: Shift, position: Int) {
            shift.let {
                if (shift.closed_at != null) {
                    itemView.date.text = shift.closed_at.substring(0,10)
                    itemView.time.text = shift.closed_at.substring(11,19)
                }else{
                    itemView.t.visibility = View.GONE
                }
                itemView.cashier.text = shift.user.first_name+" "+shift.user.last_name
                itemView.status.text = shift.status
                if (shift.status == "close") itemView.stat.setBackgroundColor(ContextCompat.getColor(context, R.color.grey_600))
                if (shift.status == "open") itemView.stat.setBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary))
            }
            current = shift
            currentPos = position
        }

    }

}