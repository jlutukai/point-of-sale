package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.ManageCategory
import com.example.pointofsale.models.Variations
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*

class ManageCategoriesAdapter(private val context: Context, private val categories: List<ManageCategory>) :
    RecyclerView.Adapter<ManageCategoriesAdapter.ManageCategoriesViewHolder>() {
    var onItemClick: ((ManageCategory) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManageCategoriesViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false)
        return ManageCategoriesViewHolder(view)
    }

    override fun getItemCount(): Int = categories.size

    override fun onBindViewHolder(holder: ManageCategoriesViewHolder, position: Int) {
        val category = categories[position]
        holder.setData(category, position)
    }

    inner class ManageCategoriesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ManageCategory? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper_category.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(product: ManageCategory?, pos: Int) {
            product?.let {
                itemView.category_name.text = product.name
                itemView.code.text = product.short_code
//                Picasso.get().load("http://165.90.23.196/uploads/img/${product.product.image!!}").into(itemView.image_product)
//                itemView.price_product.text = "KES. ${product.default_sell_price}"
            }
            current = product
            currentPos = pos
        }
    }
}