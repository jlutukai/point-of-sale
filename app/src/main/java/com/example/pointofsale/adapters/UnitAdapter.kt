package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.ManageUnit
import kotlinx.android.synthetic.main.item_category.view.*

class UnitAdapter (private val context: Context, private val units: List<ManageUnit>) :
    RecyclerView.Adapter<UnitAdapter.UnitViewHolder>() {
    var onItemClick: ((ManageUnit) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UnitViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false)
        return UnitViewHolder(view)
    }

    override fun getItemCount(): Int = units.size

    override fun onBindViewHolder(holder: UnitViewHolder, position: Int) {
        val prod = units[position]
        holder.setData(prod, position)
    }

    inner class UnitViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ManageUnit? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper_category.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(unit: ManageUnit?, pos: Int) {
            unit?.let {
                itemView.category_name.text = unit.actual_name+" "+unit.short_name
                itemView.code.visibility = View.GONE
//                Picasso.get().load("http://165.90.23.196/uploads/img/${product.image}").into(itemView.image_product)
//                itemView.price_product.text = "KES. ${product.default_sell_price}"
            }
            current = unit
            currentPos = pos
        }
    }
}