package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.Transaction
import kotlinx.android.synthetic.main.item_transaction.view.*

class TransactionAdapter(
    private val context: Context,
    private val transactions: List<Transaction>
) :
    RecyclerView.Adapter<TransactionAdapter.TransactionViewHolder>() {
    var onItemClick: ((Transaction) -> Unit)? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TransactionViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_transaction, parent, false)
        return TransactionViewHolder(view)
    }

    override fun getItemCount(): Int = transactions.size

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val trans = transactions[position]
        holder.setData(trans, position)
    }

    inner class TransactionViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Transaction? = null
        private var currentPos: Int = 0

        init {
            itemView.transaction.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }

        fun setData(trans: Transaction, position: Int) {
            trans.let {
                itemView.invoice_no.text = trans.invoice_no
                itemView.payment_status.text = trans.payment_status?.capitalize()
                itemView.date.text = trans.transaction_date.substring(0,10)
                if (trans.payment_status == "due") itemView.stat.setBackgroundColor(ContextCompat.getColor(context, R.color.red_600))
                if (trans.payment_status == "partial") itemView.stat.setBackgroundColor(ContextCompat.getColor(context, R.color.orange_A400))
                if (trans.payment_status == "paid") itemView.stat.setBackgroundColor(ContextCompat.getColor(context, R.color.saf_green))
//                itemView.waiter.text = trans.sales_person.username
            }
            current = trans
            currentPos = position
        }

    }
}