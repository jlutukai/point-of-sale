package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.Ticket
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.android.synthetic.main.item_ticket.view.*

class TicketAdapter (private val context: Context, private val tickets: MutableList<Ticket>) :
    RecyclerView.Adapter<TicketAdapter.TicketViewHolder>() {
    var onItemClickAdd: ((Ticket) -> Unit)? = null
    var onItemClickSub: ((Ticket) -> Unit)? = null
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): TicketViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_ticket, parent, false)
        return TicketViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tickets.size
    }

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        val ticket = tickets[position]
        holder.setData(ticket, position)
    }

    inner class TicketViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Ticket? = null
        private var currentPos: Int = 0

        init {
            itemView.add.setOnClickListener {
                current?.let {
                    onItemClickAdd!!.invoke(it)
                }
            }
            itemView.sub.setOnClickListener {
                current?.let {
                    onItemClickSub!!.invoke(it)
                }
            }
        }

        fun setData(ticket: Ticket, position: Int) {
            ticket.let {
                itemView.number.text = position.plus(1).toString()+". "
                itemView.item_name.text = ticket.name
                itemView.qty.text = ticket.quantity.toString()
                itemView.cost.amount = ticket.sell_price.toFloat()
                Picasso.get().load("http://165.90.23.196/uploads/img/${ticket.product.image!!}").into(itemView.image)

            }
            current = ticket
            currentPos = position
        }

    }

}