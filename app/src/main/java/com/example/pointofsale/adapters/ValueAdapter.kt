package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_value.view.*

class ValueAdapter (private val context: Context, private val values: List<String>) :
    RecyclerView.Adapter<ValueAdapter.ValueViewHolder>() {
    var onItemClick: ((String) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ValueViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_value, parent, false)
        return ValueViewHolder(view)
    }

    override fun getItemCount(): Int = values.size

    override fun onBindViewHolder(holder: ValueViewHolder, position: Int) {
        val prod = values[position]
        holder.setData(prod, position)
    }

    inner class ValueViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: String? = null
        private var currentPos: Int = 0

        init {
            itemView.delete_value.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(value: String?, pos: Int) {
            value?.let {
                itemView.number.text = pos.plus(1).toString()
                itemView.name_value.text = value
            }
            current = value
            currentPos = pos
        }
    }
}