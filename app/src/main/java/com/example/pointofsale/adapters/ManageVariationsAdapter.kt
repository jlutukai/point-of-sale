package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.ManageProduct
import com.example.pointofsale.models.ManageVariation
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_category.view.*
import kotlinx.android.synthetic.main.item_product.view.*

class ManageVariationsAdapter (private val context: Context, private val product: List<ManageVariation>) :
    RecyclerView.Adapter<ManageVariationsAdapter.VariationManagementViewHolder>() {
    var onItemClick: ((ManageVariation) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VariationManagementViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false)
        return VariationManagementViewHolder(view)
    }

    override fun getItemCount(): Int = product.size

    override fun onBindViewHolder(holder: VariationManagementViewHolder, position: Int) {
        val prod = product[position]
        holder.setData(prod, position)
    }

    inner class VariationManagementViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ManageVariation? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper_category.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(product: ManageVariation?, pos: Int) {
            product?.let {
                itemView.category_name.text = product.name
                itemView.code.visibility = View.GONE
//                Picasso.get().load("http://165.90.23.196/uploads/img/${product.image}").into(itemView.image_product)
//                itemView.price_product.text = "KES. ${product.default_sell_price}"
            }
            current = product
            currentPos = pos
        }
    }
}