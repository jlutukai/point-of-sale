package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.ProductVariationAdd
import kotlinx.android.synthetic.main.item_variation_input.view.*

class ProductVariationAdapter (private val context: Context, private val brands: List<ProductVariationAdd>) :
    RecyclerView.Adapter<ProductVariationAdapter.ProductVariationViewHolder>() {
    var onItemClickEdit: ((ProductVariationAdd) -> Unit)? = null
    var onItemClickDelete: ((ProductVariationAdd) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductVariationViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_variation_input, parent, false)
        return ProductVariationViewHolder(view)
    }

    override fun getItemCount(): Int = brands.size

    override fun onBindViewHolder(holder: ProductVariationViewHolder, position: Int) {
        val brand = brands[position]
        holder.setData(brand, position)
    }

    inner class ProductVariationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ProductVariationAdd? = null
        private var currentPos: Int = 0

        init {
            itemView.edit_variation.setOnClickListener {
                current?.let {
                    onItemClickEdit?.invoke(it)
                }
            }
            itemView.remove_variation.setOnClickListener {
                current?.let {
                    onItemClickDelete?.invoke(it)
                }
            }
        }

        fun setData(brand: ProductVariationAdd?, pos: Int) {
            brand?.let {
                itemView.name_value.text = brand.name
                itemView.sku_variation.text = brand.sku
                itemView.exc_tax.text = brand.purchase_price_exc_tax.toString()
                itemView.inc_tax.text = brand.purchase_price_inc_tax.toString()
                itemView.margin.text = brand.margin.toString()
                itemView.selling_price.text = brand.sell_price.toString()
            }
            current = brand
            currentPos = pos
        }
    }
}