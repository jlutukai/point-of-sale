package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.ManageProduct
import com.example.pointofsale.models.Variations
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*

class ProductManagementAdapter(private val context: Context, private val product: List<ManageProduct>) :
    RecyclerView.Adapter<ProductManagementAdapter.ProductManagementViewHolder>() {
    var onItemClick: ((ManageProduct) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductManagementViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false)
        return ProductManagementViewHolder(view)
    }

    override fun getItemCount(): Int = product.size

    override fun onBindViewHolder(holder: ProductManagementViewHolder, position: Int) {
        val prod = product[position]
        holder.setData(prod, position)
    }

    inner class ProductManagementViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ManageProduct? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(product: ManageProduct?, pos: Int) {
            product?.let {
                itemView.prod_name.text = product.name
                Picasso.get().load("http://165.90.23.196/uploads/img/${product.image}").into(itemView.image_product)
//                itemView.price_product.text = "KES. ${product.default_sell_price}"
            }
            current = product
            currentPos = pos
        }
    }
}