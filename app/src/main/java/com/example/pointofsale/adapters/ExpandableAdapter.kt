package com.example.pointofsale.adapters

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.example.pointofsale.R
import com.example.pointofsale.models.Nav

class ExpandableAdapter (private val context: Context, private val listParent : List<Nav>,
                         private val listChild : HashMap<Nav, List<Nav>>) : BaseExpandableListAdapter(){
    override fun getGroup(groupPosition: Int): Any = listParent[groupPosition]

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true

    override fun hasStableIds(): Boolean = false

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val convertGroupView = convertView?:run {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            inflater.inflate(R.layout.list_item_parent, null)
        }
        val parentModel = getGroup(groupPosition) as Nav
        val titleParent = convertGroupView!!.findViewById<TextView>(R.id.title_parent)
        val parentIcon = convertGroupView.findViewById<ImageView>(R.id.icon)
        titleParent.setTypeface(null, Typeface.BOLD)
        titleParent.text = parentModel.name
        parentIcon.setImageResource(parentModel.icon)
        return convertGroupView
         }

    override fun getChildrenCount(groupPosition: Int): Int = listChild[listParent[groupPosition]]?.size?:0

    override fun getChild(groupPosition: Int, childPosition: Int): Any = listChild[listParent[groupPosition]]?.get(childPosition)?:"Null"

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        val convertChildView = convertView?:run {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            inflater.inflate(R.layout.item_child_list, null)
        }
        val  childModel = getChild(groupPosition, childPosition) as Nav

        val childTitle = convertChildView.findViewById<TextView>(R.id.title_child)
        val childIcon  = convertChildView.findViewById<ImageView>(R.id.icon)
        childTitle.text = childModel.name
//        childIcon.setImageResource(childModel.icon)
        return convertChildView
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()

    override fun getGroupCount(): Int = this.listParent.size

}