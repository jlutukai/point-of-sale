package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.models.Variations
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*
import android.widget.ImageView


class ProductsAdapter (private val context: Context, private val product: List<Variations>) :
    RecyclerView.Adapter<ProductsAdapter.ProductsHolder>() {
    var onItemClick: ((Variations) -> Unit)? = null
    private var actionListener: ProductItemActionListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductsHolder {
        val view = LayoutInflater.from(context).inflate(com.example.pointofsale.R.layout.item_product, parent, false)
        return ProductsHolder(view)
    }
    fun setActionListener(actionListener: ProductItemActionListener) {
        this.actionListener = actionListener
    }
    override fun getItemCount(): Int = product.size

    override fun onBindViewHolder(holder: ProductsHolder, position: Int) {
        val prod = product[position]
        holder.setData(prod, position)
    }

    inner class ProductsHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: Variations? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper.setOnClickListener {
                var view = itemView.image_product
                current?.let {
                    onItemClick?.invoke(it)
                }
                if(actionListener!=null) actionListener!!.onItemTap(view)
            }
        }


        fun setData(product: Variations?, pos: Int) {
            product?.let {
                itemView.prod_name.text = product.name
                Picasso.get().load("http://165.90.23.196/uploads/img/${product.product.image}").into(itemView.image_product)
//                itemView.price_product.text = "KES. ${product.default_sell_price}"
            }
            current = product
            currentPos = pos
        }
    }


    interface ProductItemActionListener {
        fun onItemTap(imageView: View?)
    }
}