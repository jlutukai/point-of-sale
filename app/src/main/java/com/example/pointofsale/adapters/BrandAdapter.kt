package com.example.pointofsale.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.models.ManageBrand
import kotlinx.android.synthetic.main.item_category.view.*

class BrandAdapter (private val context: Context, private val brands: List<ManageBrand>) :
    RecyclerView.Adapter<BrandAdapter.BrandViewHolder>() {
    var onItemClick: ((ManageBrand) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false)
        return BrandViewHolder(view)
    }

    override fun getItemCount(): Int = brands.size

    override fun onBindViewHolder(holder: BrandViewHolder, position: Int) {
        val brand = brands[position]
        holder.setData(brand, position)
    }

    inner class BrandViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var current: ManageBrand? = null
        private var currentPos: Int = 0

        init {
            itemView.main_wrapper_category.setOnClickListener {
                current?.let {
                    onItemClick?.invoke(it)
                }
            }
        }


        fun setData(brand: ManageBrand?, pos: Int) {
            brand?.let {
                itemView.category_name.text = brand.name
                itemView.code.visibility = View.GONE
            }
            current = brand
            currentPos = pos
        }
    }
}