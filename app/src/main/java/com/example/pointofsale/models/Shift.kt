package com.example.pointofsale.models

data class Shift(
    val id: Int,
    val business_id: Int,
    val user_id: Int,
    val status: String,
    val closed_at: String?,
    val closing_amount: String,
    val total_card_slips: Int,
    val total_cheques: Int,
    val closing_note: String?,
    val created_at: String,
    val updated_at: String,
    val document: String,
    val user: User
)