package com.example.pointofsale.models

data class Products(
    val message: String,
    val variations: List<Variations>,
    val products: List<Product>
)