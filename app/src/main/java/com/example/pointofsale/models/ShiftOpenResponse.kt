package com.example.pointofsale.models

data class ShiftOpenResponse(
    val message: String
)