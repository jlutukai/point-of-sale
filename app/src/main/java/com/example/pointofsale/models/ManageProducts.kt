package com.example.pointofsale.models

data class ManageProducts(
    val products : List<ManageProduct>
)