package com.example.pointofsale.models

data class OnReceiverEvent(val phoneNumber: String, val code : String)