package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "asset")
data class Asset(
    @PrimaryKey val id : Int,
    val serial : String,
    val rfid : String?,
    val initial_value : String,
    val batch_id : Int,
    val current_station_id : Int?,
    val current_contact_id : Int?,
    val in_company : Int
){

}