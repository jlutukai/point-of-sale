package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contacts")
data class Contact(
    @PrimaryKey
    val id: Int,
    val name: String,
    val email: String?,
    val mobile: String,
    val supplier_business_name: String?,
    val contact_id : String?,
    val customer_group_id : Int?,
    val updated_at: String
) {
    constructor() : this(0,"","","","","",0,"")
}