package com.example.pointofsale.models

data class ManageVariations (
    val variations : List<ManageVariation>
)