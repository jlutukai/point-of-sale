package com.example.pointofsale.models

data class Assets(
    val assets : List<Asset>
)