package com.example.pointofsale.models

data class Contacts(
    val contacts: List<Contact>
)