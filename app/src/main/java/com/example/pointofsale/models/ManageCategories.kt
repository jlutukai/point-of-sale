package com.example.pointofsale.models

data class ManageCategories(
    val categories : List<ManageCategory>
)