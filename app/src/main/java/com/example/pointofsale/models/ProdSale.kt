package com.example.pointofsale.models

data class ProdSale(
    val unit_price: Double,
    val line_discount_type: String,
    val line_discount_amount: Double,
    val item_tax: Double,
    val tax_id: String?,
    val sell_line_note: String?,
    val product_id: Int,
    val variation_id: Int,
    val enable_stock: Int,
    val quantity: Double,
    val assets: List<String>,
    val product_unit_id: Int,
    val base_unit_multiplier: String,
    val unit_price_inc_tax: Double
)