package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "stations"
)
data class Station(
    @PrimaryKey
    val id : Int,
    val name : String,
    val landmark : String,
    val business_id : Int
){
}