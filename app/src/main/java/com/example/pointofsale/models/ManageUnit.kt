package com.example.pointofsale.models

data class ManageUnit(
    val id: Int,
    val business_id: Int,
    val actual_name: String,
    val short_name: String,
    val allow_decimal: Int,
    val base_unit_id: String?,
    val base_unit_multiplier: String?,
    val created_by: Int,
    val deleted_at: String?,
    val created_at: String,
    val updated_at: String
)