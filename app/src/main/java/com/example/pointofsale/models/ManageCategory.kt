package com.example.pointofsale.models

data class ManageCategory(
    val id: Int,
    val name: String,
    val business_id: Int,
    val short_code: String?,
    val parent_id: Int,
    val created_by: Int,
    val deleted_at: String?,
    val created_at: String,
    val updated_at: String,
    val is_unstructured: Int,
    val allow_self: Int
)