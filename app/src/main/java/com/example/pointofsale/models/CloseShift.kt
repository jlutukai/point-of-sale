package com.example.pointofsale.models

data class CloseShift(
    val closing_amount: String,
    val total_card_slips: String?,
    val total_cheques: String?,
    val closing_note: String?
)