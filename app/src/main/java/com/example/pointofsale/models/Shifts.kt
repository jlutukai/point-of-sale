package com.example.pointofsale.models

data class Shifts(
    val shifts: List<Shift>
)