package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "products")
data class Product(
    @PrimaryKey val id: Int,
    val name: String,
    val type : String,
    val category_id : Int,
    val tax_type  : String,
    val unit_id : Int,
    val enable_stock : Int,
    val enable_sr_no : Int,
    val brand_id : Int,
    val alert_quantity : Int,
    val image : String?,
    val default_purchase_price: Double,
    val default_sell_price: Double
) { constructor() : this(0, "", "",0,"",0,0, 0,0,0,"",0.00,0.00)}
