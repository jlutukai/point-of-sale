package com.example.pointofsale.models

data class ManageVariation(
    val id : Int,
    val name : String,
    val total_pv : Int,
    val values : List<Value>
)