package com.example.pointofsale.models

data class ResponsePost(
    val message: String
)