package com.example.pointofsale.models

//import androidx.room.Entity
//import androidx.room.PrimaryKey
//
//@Entity(tableName = "sellline")
data class SellLine(
//    @PrimaryKey
    val idv: Int,
    val transaction_id: Int,
    val product_id: Int,
    val variation_id: Int,
    val quantity: Int,
    val quantity_returned: String?,
    val unit_price_before_discount: String?,
    val unit_price: String?,
    val line_discount_type: String?,
    val line_discount_amount: String?,
    val unit_price_inc_tax: String?,
    val item_tax: String?,
    val tax_id: String?,
    val discount_id: String?,
    val lot_no_line_id: String?,
    val sell_line_note: String?,
    val res_service_staff_id: String?,
    val res_line_order_status: String?,
    val parent_sell_line_id: String?,
    val sub_unit_id: String?,
    val created_at: String,
    val updated_at: String
)
//{
//    constructor() : this(0,0,0,0,0,"","","",""
//    ,"","","","","","","","","","","","","")
//}