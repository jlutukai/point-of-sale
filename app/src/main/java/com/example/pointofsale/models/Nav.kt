package com.example.pointofsale.models


data class Nav(
    val name : String,
    val icon : Int
)