package com.example.pointofsale.models

//import androidx.room.Entity
//import androidx.room.PrimaryKey
//
//@Entity(tableName = "paymentline")
data class PaymentLine(
//    @PrimaryKey
    val id: Int,
    val transaction_id: Int,
    val business_id: Int,
    val is_return: Int,
    val amount: String,
    val method: String,
    val transaction_no: String?,
    val card_transaction_number: String?,
    val card_number: String?,
    val card_type: String?,
    val card_holder_name: String?,
    val card_month: String?,
    val card_year: String?,
    val card_security: String?,
    val cheque_number: String?,
    val bank_account_number: String?,
    val paid_on: String?,
    val created_by: Int,
    val payment_for: Int,
    val parent_id: String?,
    val note: String?,
    val document: String?,
    val payment_ref_no: String?,
    val account_id: String?,
    val created_at: String,
    val updated_at: String,
    val reconcile_id: Int
)
//{
//    constructor() : this(0,0,0,0,"","","","","","","","","","","","","",0,0,"","","","","","","",0)
//}