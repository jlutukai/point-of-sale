package com.example.pointofsale.models

data class AddVariation(
    val name : String,
    val values : List<String>
)