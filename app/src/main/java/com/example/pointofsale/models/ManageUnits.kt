package com.example.pointofsale.models

data class ManageUnits(
    val units : List<ManageUnit>
)