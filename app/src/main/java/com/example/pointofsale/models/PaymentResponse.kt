package com.example.pointofsale.models

import sunmi.sunmiui.dialog.CodeInputSuccess

data class PaymentResponse(
    val status_code : Int,
    val transaction: Transaction,
    val success: Int,
    val msg : String,
    val url : String
)