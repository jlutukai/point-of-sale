package com.example.pointofsale.models

data class Value(
    val id : Int,
    val name : String,
    val variation_template_id : Int,
    val created_at : String,
    val updated_at : String
)