package com.example.pointofsale.models

data class ManageBrand(
    val id: Int,
    val business_id: Int,
    val name: String,
    val description: String,
    val created_by: Int,
    val deleted_at: String?,
    val created_at: String,
    val updated_at: String
)