package com.example.pointofsale.models

data class AddCategory(
    val name : String,
    val short_code : String,
    val parent_id : Int?
)