package com.example.pointofsale.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.example.pointofsale.room.converters.AssetsConverter
import com.example.pointofsale.room.converters.ProductConverter

@TypeConverters(ProductConverter::class, AssetsConverter::class)
@Entity(tableName = "ticket")
data class Ticket(
    @PrimaryKey
    @ColumnInfo(name = "id") val id: Int,
    val name: String,
    val product_id: Int,
    val default_sell_price: String,
    val default_purchase_price: String,
    val dpp_inc_tax: String,
    val sell_price_inc_tax: String,
    val product: Product,
    val assets : MutableList<String>,
    var sell_price: Double,
    var quantity: Int
){
}