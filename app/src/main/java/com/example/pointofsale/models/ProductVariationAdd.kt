package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "productvariationadd")
data class ProductVariationAdd(
    @PrimaryKey val id: Int,
    val name : String,
    val sku : String?,
    val purchase_price_exc_tax : Double,
    val purchase_price_inc_tax : Double,
    val margin : Double,
    val sell_price : Double
){
    constructor() : this(id = 0,name = "", sku = "", purchase_price_exc_tax = 0.0, purchase_price_inc_tax = 0.0, margin = 0.0 , sell_price = 0.0)
}