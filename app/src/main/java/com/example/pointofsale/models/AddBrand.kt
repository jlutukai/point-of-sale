package com.example.pointofsale.models


data class AddBrand(
    val name : String,
    val description: String?
)