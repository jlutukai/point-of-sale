package com.example.pointofsale.models

data class Token(
    val token : String,
    val user : User,
    val stations : List<Station>
)