package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_data")
data class User(
    @PrimaryKey
    val id : Int,
    val surname : String,
    val first_name : String,
    val last_name : String,
    val username : String,
    val is_customer : Int,
    val business_id : Int
){constructor(): this(id = 0,surname = "",first_name = "",last_name = "",username = "",is_customer = 0,business_id = 0)}