package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "business_details")
data class BusinessDetails(
    @PrimaryKey
    val id : Int,
    val name : String,
    val tax_number_1 : String?,
    val tax_number_2 :  String?,
    val sell_price_tax  :String?,
    val default_sales_discount : String?,
    val default_profit_percent: Double
){}