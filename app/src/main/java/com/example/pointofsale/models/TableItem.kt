package com.example.pointofsale.models

data class TableItem(
    val text: Array<String>,
    var width: IntArray,
    var align: IntArray
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TableItem

        if (!text.contentEquals(other.text)) return false
        if (!width.contentEquals(other.width)) return false
        if (!align.contentEquals(other.align)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = text.contentHashCode()
        result = 31 * result + width.contentHashCode()
        result = 31 * result + align.contentHashCode()
        return result
    }
}
