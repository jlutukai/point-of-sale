package com.example.pointofsale.models

data class Cashier(
    val cashiers : List<User>,
    val status_code : Int
)