package com.example.pointofsale.models

import com.google.gson.annotations.SerializedName

data class Message(
    val success : Int,
    val msg : String,
    @SerializedName("Served Bt")
    val Servedby :String,
    val receipt_no : String,
    val error : String,
    val message : String
)