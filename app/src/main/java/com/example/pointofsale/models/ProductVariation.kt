package com.example.pointofsale.models

data class ProductVariation(
    val id : Int,
    val variation_template_id : Int?,
    val product_id : Int,
    val is_dummy : Int,
    val created_at : String,
    val updated_at : String
)