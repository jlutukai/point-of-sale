package com.example.pointofsale.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "categories")
data class Category(
    @PrimaryKey
    val id: Int,
    val name: String,
    val business_id: Int,
    val short_code: Int,
    val parent_id: Int?,
    val created_by: Int,
    val updated_at: String,
    val is_unstructured: Int,
    val allow_self: Int
) {}