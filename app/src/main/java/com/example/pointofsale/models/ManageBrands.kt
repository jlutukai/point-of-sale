package com.example.pointofsale.models

data class ManageBrands(
    val brands : List<ManageBrand>
)