package com.example.pointofsale.models

data class TransactionsResponse(
    val message: String,
    val transactions  :List<Transaction>
)