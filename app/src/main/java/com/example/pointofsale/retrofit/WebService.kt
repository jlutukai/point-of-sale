package com.example.pointofsale.retrofit

import com.example.pointofsale.models.*
import retrofit2.Response
import retrofit2.http.*

interface WebService {

    @POST("login/")
    suspend fun loginUser(@Query("username") username: String, @Query("password") password: String)
            : Response<Token>

    @GET("categories")
    suspend fun fetchCategories(@Header("authorization") authorization: String): Response<Categories>

    @GET("products")
    suspend fun fetchProducts(@Header("authorization") authorization: String): Response<Products>

    @GET("contacts")
    suspend fun fetchContacts(@Header("authorization") authorization: String): Response<Contacts>

    @GET("get_business/{id}")
    suspend fun getBusnessDetails(@Header("authorization") authorization: String,
                                  @Path("id") id: String) : Response<BusinessDetails>

    @POST("pos/create/invoice")
    suspend fun makePayment(@Header("authorization") authorization: String,
                            @Header("accept") accept: String,
                            @Body invoices: Invoices) : Response<PaymentResponse>

    @GET("transactions")
    suspend fun getTransactions(@Header("authorization") authorization: String) : Response<TransactionsResponse>

    @GET("user/cashiers")
    suspend fun getCashiers(@Header("authorization") authorization: String) : Response<Cashier>

    @GET("shifts")
    suspend fun getShifts(@Header("authorization") authorization: String) : Response<Shifts>

    @POST("close/shifts/{id}")
    suspend fun closeShift(@Header("authorization") authorization: String,
                           @Path("id") id: String,
                           @Body shift: CloseShift) : Response<Shift>

    @POST("open/shifts")
    suspend fun openShift(@Header("authorization") authorization: String,
                          @Body shift: OpenShift) : Response<ShiftOpenResponse>

    @GET("managecategories")
    suspend fun getManageCategories(@Header("authorization") authorization: String) : Response<ManageCategories>

    @POST("managecategories")
    suspend fun addCategories(@Header("authorization") authorization: String, @Body addCategory: AddCategory) : Response<ResponsePost>

    @PATCH("managecategories")
    suspend fun editCategories(@Header("authorization") authorization: String, @Body addCategory: AddCategory) : Response<ResponsePost>

    @DELETE("managecategories/{id}")
    suspend fun deleteCategory(@Header("authorization") authorization: String, @Path("id") id : Int) : Response<Message>

    @GET("productmanagements")
    suspend fun getManageProducts(@Header("authorization") authorization: String) : Response<ManageProducts>

    @GET("managevariationtemplates")
    suspend fun getManageVaiations(@Header("authorization") authorization: String) : Response<ManageVariations>

    @POST("managevariationtemplates")
    suspend fun addVaiations(@Header("authorization") authorization: String, @Body addVariation: AddVariation) : Response<ResponsePost>

    @GET("managebrands")
    suspend fun getManageBrands(@Header("authorization") authorization: String) : Response<ManageBrands>

    @POST("managebrands")
    suspend fun addBrands(@Header("authorization") authorization: String, @Body addBrand: AddBrand) : Response<ResponsePost>

    @GET("manageunits")
    suspend fun getManageUnits(@Header("authorization") authorization: String) : Response<ManageUnits>

    @GET("asset_types")
    suspend fun fetchAssets(@Header("authorization") authorization: String): Response<Assets>
}