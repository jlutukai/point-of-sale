package com.example.pointofsale.reciever

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.telephony.SmsMessage
import android.util.Log
import com.example.pointofsale.models.OnReceiverEvent
import org.greenrobot.eventbus.EventBus

class SmsListener : BroadcastReceiver() {
    private var currentSMS: SmsMessage? = null
    private var message: String? = null
    var code : String = ""

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent!!.action.equals("android.provider.Telephony.SMS_RECEIVED")) {
            val bundle = intent.extras //---get the SMS message passed in---
            if (bundle != null) { //---retrieve the SMS message received---
                try {
                    val pdus =
                        bundle["pdus"] as Array<*>?
                    if (pdus != null) {
                        for (i in pdus) {

                            currentSMS = getIncomingMessage(i!!, bundle)

                            val senderNo = currentSMS!!.displayOriginatingAddress

                            message = currentSMS!!.displayMessageBody
                            code  = message!!.substring(0,10)
                            if (senderNo == "MPESA"){
                                EventBus.getDefault().post(OnReceiverEvent(phoneNumber = senderNo, code = code))
                            }
                            Log.d("sms : ", "senderNum: $senderNo :\n message: $code")
//                            abortBroadcast()
                            break
                        }
                    }
                } catch (e: Exception) { //
                    Log.d("Exception caught","${e.message}")
                }
            }
        }
    }
    private fun getIncomingMessage(
        aObject: Any,
        bundle: Bundle
    ): SmsMessage? {
        val currentSMS: SmsMessage
        currentSMS = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val format = bundle.getString("format")
            SmsMessage.createFromPdu(aObject as ByteArray, format)
        } else {
            @Suppress("DEPRECATION")
            SmsMessage.createFromPdu(aObject as ByteArray)
        }
        return currentSMS
    }

}