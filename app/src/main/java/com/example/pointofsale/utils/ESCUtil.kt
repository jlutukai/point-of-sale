package com.example.pointofsale.utils

import android.R
import android.graphics.Bitmap
import android.widget.Toast
import java.io.ByteArrayOutputStream


class ESCUtil{
    val ESC: Byte = 0x1B // Escape
    val FS: Byte = 0x1C // Text delimiter
    val GS: Byte = 0x1D // Group separator
    val DLE: Byte = 0x10 // data link escape
    val EOT: Byte = 0x04 // End of transmission
    val ENQ: Byte = 0x05 // Enquiry character
    val SP: Byte = 0x20 // Spaces
    val HT: Byte = 0x09 // Horizontal list
    val LF: Byte = 0x0A //Print and wrap (horizontal orientation)
    val CR: Byte = 0x0D // Home key
    val FF: Byte = 0x0C // Carriage control (print and return to the standard mode (in page mode))
    val CAN: Byte = 0x18 // Canceled (cancel print data in page mode)


    fun init_printer(): ByteArray? {
        val result = ByteArray(2)
        result[0] = ESC
        result[1] = 0x40
        return result
    }

    fun setPrinterDarkness(value: Int): ByteArray? {
        val result = ByteArray(9)
        result[0] = GS
        result[1] = 40
        result[2] = 69
        result[3] = 4
        result[4] = 0
        result[5] = 5
        result[6] = 5
        result[7] = (value shr 8).toByte()
        result[8] = value.toByte()
        return result
    }

    fun getPrintQRCode(
        code: String?,
        modulesize: Int,
        errorlevel: Int
    ): ByteArray? {
        val buffer = ByteArrayOutputStream()
        try {

        } catch (e: Exception) {
            e.printStackTrace()
        }
        return buffer.toByteArray()
    }

    fun getPrintDoubleQRCode(
        code1: String,
        code2: String,
        modulesize: Int,
        errorlevel: Int
    ): ByteArray? {
        val buffer = ByteArrayOutputStream()
        try {

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return buffer.toByteArray()
    }

    /**
     * 光栅打印二维码
     */
//    fun getPrintQRCode2(data: String?, size: Int): ByteArray? {
//        val bytes1 = ByteArray(4)
//        bytes1[0] = GS
//        bytes1[1] = 0x76
//        bytes1[2] = 0x30
//        bytes1[3] = 0x00
//        val bytes2: ByteArray = BytesUtil.getZXingQRCode(data, size)
//        return BytesUtil.byteMerger(bytes1, bytes2)
//    }

    /**
     * 打印一维条形码
     */
    fun getPrintBarCode(
        data: String,
        symbology: Int,
        height: Int,
        width: Int,
        textposition: Int
    ): ByteArray? {
        var height = height
        var width = width
        var textposition = textposition
        if (symbology < 0 || symbology > 10) {
            return byteArrayOf(LF)
        }
        if (width < 2 || width > 6) {
            width = 2
        }
        if (textposition < 0 || textposition > 3) {
            textposition = 0
        }
        if (height < 1 || height > 255) {
            height = 162
        }
        val buffer = ByteArrayOutputStream()
        try {
            buffer.write(
                byteArrayOf(
                    0x1D, 0x66, 0x01, 0x1D, 0x48, textposition.toByte(),
                    0x1D, 0x77, width.toByte(), 0x1D, 0x68, height.toByte(), 0x0A
                )
            )
//            val barcode: ByteArray
//            barcode = if (symbology == 10) {
//                BytesUtil.getBytesFromDecString(data)
//            } else {
                data.toByteArray(charset("GB18030"))
//            }
            if (symbology > 7) {
                buffer.write(
                    byteArrayOf(
                        0x1D,
                        0x6B,
                        0x49,
//                        (barcode.size + 2).toByte(),
                        0x7B,
                        (0x41 + symbology - 8).toByte()
                    )
                )
            } else {
                buffer.write(
                    byteArrayOf(
                        0x1D,
                        0x6B,
                        (symbology + 0x41).toByte()
//                        barcode.size.toByte()
                    )
                )
            }
//            buffer.write(barcode)
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return buffer.toByteArray()
    }


    //光栅位图打印
//    fun printBitmap(bitmap: Bitmap?): ByteArray? {
//        val bytes1 = ByteArray(4)
//        bytes1[0] = GS
//        bytes1[1] = 0x76
//        bytes1[2] = 0x30
//        bytes1[3] = 0x00
//        val bytes2: ByteArray = BytesUtil.getBytesFromBitMap(bitmap)
//        return BytesUtil.byteMerger(bytes1, bytes2)
//    }

    //光栅位图打印 设置mode
//    fun printBitmap(bitmap: Bitmap?, mode: Int): ByteArray? {
//        val bytes1 = ByteArray(4)
//        bytes1[0] = GS
//        bytes1[1] = 0x76
//        bytes1[2] = 0x30
//        bytes1[3] = mode.toByte()
//        val bytes2: ByteArray = BytesUtil.getBytesFromBitMap(bitmap)
//        return BytesUtil.byteMerger(bytes1, bytes2)
//    }

    //光栅位图打印
//    fun printBitmap(bytes: ByteArray?): ByteArray? {
//        val bytes1 = ByteArray(4)
//        bytes1[0] = GS
//        bytes1[1] = 0x76
//        bytes1[2] = 0x30
//        bytes1[3] = 0x00
//        return BytesUtil.byteMerger(bytes1, bytes)
//    }


    /*
	*	选择位图指令 设置mode
	*	需要设置1B 33 00将行间距设为0
	 */
//    fun selectBitmap(bitmap: Bitmap?, mode: Int): ByteArray? {
//        return BytesUtil.byteMerger(
//            BytesUtil.byteMerger(
//                byteArrayOf(0x1B, 0x33, 0x00),
//                BytesUtil.getBytesFromBitMap(bitmap, mode)
//            ), byteArrayOf(0x0A, 0x1B, 0x32)
//        )
//    }



    fun nextLine(lineNum: Int): ByteArray? {
        val result = ByteArray(lineNum)
        for (i in 0 until lineNum) {
            result[i] = LF
        }
        return result
    }

    // ------------------------underline-----------------------------
//设置下划线1点
    fun underlineWithOneDotWidthOn(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 45
        result[2] = 1
        return result
    }

    //设置下划线2点
    fun underlineWithTwoDotWidthOn(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 45
        result[2] = 2
        return result
    }

    //取消下划线
    fun underlineOff(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 45
        result[2] = 0
        return result
    }

    // ------------------------bold-----------------------------
    // ------------------------bold-----------------------------
    /**
     * 字体加粗
     */
    fun boldOn(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 69
        result[2] = 0xF
        return result
    }

    /**
     * 取消字体加粗
     */
    fun boldOff(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 69
        result[2] = 0
        return result
    }

    // ------------------------character-----------------------------
/*
	*单字节模式开启
	 */
    fun singleByte(): ByteArray? {
        val result = ByteArray(2)
        result[0] = FS
        result[1] = 0x2E
        return result
    }

    /*
	*单字节模式关闭
 	*/
    fun singleByteOff(): ByteArray? {
        val result = ByteArray(2)
        result[0] = FS
        result[1] = 0x26
        return result
    }

    /**
     * 设置单字节字符集
     */
    fun setCodeSystemSingle(charset: Byte): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 0x74
        result[2] = charset
        return result
    }

    /**
     * 设置多字节字符集
     */
    fun setCodeSystem(charset: Byte): ByteArray? {
        val result = ByteArray(3)
        result[0] = FS
        result[1] = 0x43
        result[2] = charset
        return result
    }

    // ------------------------Align-----------------------------

    // ------------------------Align-----------------------------
    /**
     * 居左
     */
    fun alignLeft(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 97
        result[2] = 0
        return result
    }

    /**
     * 居中对齐
     */
    fun alignCenter(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 97
        result[2] = 1
        return result
    }

    /**
     * 居右
     */
    fun alignRight(): ByteArray? {
        val result = ByteArray(3)
        result[0] = ESC
        result[1] = 97
        result[2] = 2
        return result
    }

    //切刀
    fun cutter(): ByteArray? {
        return byteArrayOf(0x1d, 0x56, 0x01)
    }

    //走纸到黑标
    fun gogogo(): ByteArray? {
        return byteArrayOf(0x1C, 0x28, 0x4C, 0x02, 0x00, 0x42, 0x31)
    }


    ////////////////////////////////////////////////////////////////////////////////////
//////////////////////////          private                /////////////////////////
////////////////////////////////////////////////////////////////////////////////////

}