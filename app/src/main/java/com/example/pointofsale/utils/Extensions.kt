package com.example.pointofsale.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.util.TypedValue
import android.widget.Toast
import kotlin.math.roundToInt

private lateinit var sessionManager: SharedPreferences
private lateinit var openingBalance: SharedPreferences
private lateinit var loginData: SharedPreferences


fun Context.storeToken(token: String) {
    sessionManager = this.getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
        ?: return
    with(sessionManager.edit()) {
        putString("tokenVal", token)
        apply()
    }
}

fun Context.getToken(): String {
    sessionManager = this.getSharedPreferences("TOKEN", Context.MODE_PRIVATE)
    val token = sessionManager.getString("tokenVal", null)
    return token?.let {
        return@let token
    }.toString()
}
fun Context.storeOpenningbalance(opb:String, stid:String){
    openingBalance = this.getSharedPreferences("Categories", Context.MODE_PRIVATE)
        ?:return
    with(openingBalance.edit()){
        putString("id",opb)
        putString("stid",stid)
        apply()
    }
}
fun Context.getOpeningBalance() : String{
    openingBalance = this.getSharedPreferences("Categories", Context.MODE_PRIVATE)
    val id = openingBalance.getString("id", null)
    return id?.let {
        return@let id
    }.toString()
}
fun Context.getStationId() : String{
    openingBalance = this.getSharedPreferences("Categories", Context.MODE_PRIVATE)
    val id = openingBalance.getString("stid", null)
    return id?.let {
        return@let id
    }.toString()
}

fun clearOpeningBalance() {
    openingBalance.edit().clear().apply()
}

fun Context.storeUsername(username:String, pass : String){
    loginData = this.getSharedPreferences("Login", Context.MODE_PRIVATE)
        ?:return
    with(loginData.edit()){
        putString("uname",username)
        putString("pass", pass)
        apply()
    }
}
fun Context.getUname() : String{
    loginData = this.getSharedPreferences("Login", Context.MODE_PRIVATE)
    val id = loginData.getString("uname", null)
    return id?.let {
        return@let id
    }.toString()
}

fun Context.getPass() : String{
    loginData = this.getSharedPreferences("Login", Context.MODE_PRIVATE)
    val id = loginData.getString("pass", null)
    return id?.let {
        return@let id
    }.toString()
}

fun clearLoginDetails() {
    loginData.edit().clear().apply()
}

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun dpToPx(c: Context, dp: Int): Int {
    val r = c.resources
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics).roundToInt()
}

//network connection check
fun isNetworkConnected(context: Context): Boolean {
    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
    return activeNetwork?.isConnected == true
}