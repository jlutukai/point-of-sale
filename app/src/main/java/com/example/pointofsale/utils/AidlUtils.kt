package com.example.pointofsale.utils

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.os.Handler
import android.os.IBinder
import android.os.RemoteException
import android.util.Log
import com.example.pointofsale.models.TableItem
import com.example.pointofsale.room.RoomDbInit
import sunmi.paylib.SunmiPayKernel
import woyou.aidlservice.jiuiv5.ICallback
import woyou.aidlservice.jiuiv5.IWoyouService
import java.lang.Exception
import kotlin.collections.ArrayList


class AidlUtils {
    private val servicePackage: String = "woyou.aidlservice.jiuiv5"
    private val serviceAction = "woyou.aidlservice.jiuiv5.IWoyouService"
    private var woyouService: IWoyouService? = null
    private lateinit var mSMPayKernel : SunmiPayKernel

    private lateinit var mAidlUtil: AidlUtils
    private var context: Context? = null

    companion object {
        var escUtil: ESCUtil = ESCUtil()
    }

    fun getInstance(): AidlUtils {
        mAidlUtil = AidlUtils()
        return mAidlUtil
    }






    fun connectPrinterService(context: Context) {
        this.context = context.applicationContext
        val intent = Intent()
        intent.setPackage(servicePackage)
        intent.action = serviceAction
        context.applicationContext.startService(intent)
        context.applicationContext
            .bindService(intent, connService, Context.BIND_AUTO_CREATE)
    }

    fun disconnectPrinterService(context: Context) {
        if (woyouService != null) {
            context.applicationContext.unbindService(connService)
            woyouService = null
        }
    }

    fun isConnect(): Boolean {
        return woyouService != null
    }

    private val connService: ServiceConnection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName) {
            woyouService = null
        }

        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            woyouService = IWoyouService.Stub.asInterface(service)
        }
    }

    fun generateCB(printerCallback: ICallback): ICallback? {
        return object : ICallback.Stub() {
            @Throws(RemoteException::class)
            override fun onRunResult(isSuccess: Boolean) {
            }

            @Throws(RemoteException::class)
            override fun onReturnString(result: String) {
            }

            @Throws(RemoteException::class)
            override fun onRaiseException(code: Int, msg: String) {
            }

            @Throws(RemoteException::class)
            override fun onPrintResult(code: Int, msg: String) {
            }
        }
    }

    private val darkness: List<Int> = listOf(
        0x0600, 0x0500, 0x0400, 0x0300, 0x0200, 0x0100, 0,
        0xffff, 0xfeff, 0xfdff, 0xfcff, 0xfbff, 0xfaff
    )

    fun setDarkness(index: Int) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        val k = darkness[index]
        try {
            woyouService!!.sendRAWData(escUtil.setPrinterDarkness(k), null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun getPrinterInfo(): List<String>? {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return null
        }
        val info: MutableList<String> = ArrayList()
        try {
            info.add(woyouService!!.printerSerialNo)
            info.add(woyouService!!.printerModal)
            info.add(woyouService!!.printerVersion)
            info.add(woyouService!!.printedLength.toString() + "")
            info.add("")
            //info.add(woyouService.getServiceVersion());
            val packageManager = context!!.packageManager
            try {
                val packageInfo: PackageInfo = packageManager.getPackageInfo(servicePackage, 0)
                if (packageInfo != null) {
                    info.add(packageInfo.versionName)
                    info.add(packageInfo.versionCode.toString() + "")
                } else {
                    info.add("")
                    info.add("")
                }
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
        return info
    }

    fun initPrinter(context: Context) {
        if (woyouService == null) {
            connectPrinterService(context)
//            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            woyouService!!.printerInit(null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun printQr(data: String?, modulesize: Int, errorlevel: Int) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            woyouService!!.setAlignment(1, null)
            woyouService!!.printQRCode(data, modulesize, errorlevel, null)
            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun printBarCode(
        data: String?,
        symbology: Int,
        height: Int,
        width: Int,
        textposition: Int
    ) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            woyouService!!.printBarCode(data, symbology, height, width, textposition, null)
            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun printTitleText(content: String?, size : Float, bold: Boolean, underline:Boolean) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            if (bold) {
                woyouService!!.sendRAWData(escUtil.boldOn(), null)
            }else{
                woyouService!!.sendRAWData(escUtil.boldOff(), null)
            }
            if (underline){
            woyouService!!.sendRAWData(escUtil.underlineWithOneDotWidthOn(), null)
            }else{
                woyouService!!.sendRAWData(escUtil.underlineOff(), null)
            }
            woyouService!!.sendRAWData(escUtil.alignCenter(), null)
            woyouService!!.printTextWithFont(content, null, size, null)
            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun printText(
        content: String?
    ) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
                woyouService!!.sendRAWData(escUtil.boldOff(), null)
                woyouService!!.sendRAWData(escUtil.underlineOff(), null)
            woyouService!!.sendRAWData(escUtil.alignLeft(), null)
            woyouService!!.printText(content, null)
            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun printBitmap(bitmap: Bitmap?) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            woyouService!!.setAlignment(1, null)
            woyouService!!.printBitmap(bitmap, null)
            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun printBitmap(bitmap: Bitmap?, orientation: Int) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            if (orientation == 0) {
                woyouService!!.printBitmap(bitmap, null)
                woyouService!!.printBitmap(bitmap, null)
            } else {
                woyouService!!.printBitmap(bitmap, null)
                woyouService!!.printBitmap(bitmap, null)
            }
            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun print3Line() {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            woyouService!!.lineWrap(3, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }


    fun printTable(list: MutableList<TableItem>) {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return
        }
        try {
            for (tableItem in list) {
                Log.i(
                    "kaltin",
                    "printTable: " + tableItem!!.text[0]
                )
                woyouService!!.printColumnsText(
                    tableItem.text,
                    tableItem.width,
                    tableItem.align,
                    null
                )
            }
//            woyouService!!.lineWrap(1, null)
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    fun getPrintMode(): Int {
        if (woyouService == null) {
            context!!.showToast("The service has been disconnected!")
            return -1
        }
        val res: Int
        res = try {
            woyouService!!.printerMode
        } catch (e: RemoteException) {
            e.printStackTrace()
            -1
        }
        return res
    }

}