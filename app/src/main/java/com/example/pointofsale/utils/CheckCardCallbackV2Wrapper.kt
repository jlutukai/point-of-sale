package com.example.pointofsale.utils

import android.os.Bundle
import android.os.RemoteException
import com.sunmi.pay.hardware.aidlv2.readcard.CheckCardCallbackV2

open class CheckCardCallbackV2Wrapper : CheckCardCallbackV2.Stub() {
    override fun findMagCard(p0: Bundle?) {

    }

    override fun findRFCardEx(p0: Bundle?) {

   }

    override fun onErrorEx(p0: Bundle?) {

    }

    override fun findICCardEx(p0: Bundle?) {

    }

    override fun findICCard(p0: String?) {

    }

    override fun findRFCard(p0: String?) {

    }

    override fun onError(p0: Int, p1: String?) {

    }

}