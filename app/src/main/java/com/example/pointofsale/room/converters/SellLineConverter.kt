package com.example.pointofsale.room.converters

import androidx.room.TypeConverter
import com.example.pointofsale.models.SellLine
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type

class SellLineConverter : Serializable{

    @TypeConverter
    fun fromListToStringS(sellLine : List<SellLine>): String {
        val gson = Gson()
        val type = object : TypeToken<List<SellLine?>?>() {}.type
        return gson.toJson(sellLine, type)
    }

    @TypeConverter
    fun fromStringToListS(sellLine: String): List<SellLine> {
        val gson = Gson()
        val type: Type = object : TypeToken<List<SellLine?>?>() {}.type
        return gson.fromJson(sellLine, type)
    }
}