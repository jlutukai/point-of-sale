package com.example.pointofsale.room.repository

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import com.example.pointofsale.models.*
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.AppDatabase
import com.example.pointofsale.room.RoomDbInit
import com.example.pointofsale.room.converters.AssetsConverter
import com.example.pointofsale.utils.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class GeneralRepository {
    private val db: AppDatabase = RoomDbInit.database

    //contacts
    fun apiContacts(application: Application) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchContacts(application.getToken())
                if (response.isSuccessful) {
                    val contact = response.body()
                    contact?.let {
                        db.generalDao().nukeContacts()
                        db.generalDao().saveContactLocal(it.contacts)
                    }
                }
            } catch (e: IOException) {
                //errors
            }
        }
    }

    fun localContacts(): LiveData<List<Contact>> {
        return db.generalDao().getLocalContacts()
    }


    // login user
    fun apiLoginUser(application: Application, username: String, password: String) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.loginUser(username, password)
                if (response.isSuccessful) {
                    val token = response.body()
                    token?.let {
                        db.generalDao().nukeUserData()
                        db.generalDao().saveUserData(user = token.user)
                        db.generalDao().nukeStattions()
                        db.generalDao().saveStations(station = token.stations)
                        Log.d("stattions", "saved : ${token.stations.size}")
                    }

                }
//                else {
//                    val gson = Gson()
//                    val errorResponse = gson.fromJson<Message>(
//                        response.errorBody()!!.charStream(),
//                        Message::class.java
//                    )
//                    val msg = errorResponse.error
//                    application.showToast(msg)
//                }
            } catch (e: IOException) {
                //error
//                application.showToast("Error : ${e.message}")
            }
        }
    }
    fun saveUserData(application: Application, username: String, password: String) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.loginUser(username, password)
                if (response.isSuccessful) {
                    val token = response.body()
                    token?.let {
                        db.generalDao().nukeUserData()
                        db.generalDao().saveUserData(user = token.user)
                        db.generalDao().nukeStattions()
                        db.generalDao().saveStations(station = token.stations)
                        Log.d("stattions", "saved : ${token.stations.size}")
                    }

                } else {
                    val gson = Gson()
                    val errorResponse = gson.fromJson<Message>(
                        response.errorBody()!!.charStream(),
                        Message::class.java
                    )
                    val msg = errorResponse.error
                    application.showToast(msg)
                }
            } catch (e: IOException) {
                //error
            }
        }
    }
    fun localGetUserData():LiveData<User>{
        return db.generalDao().getUserData()
    }
    fun localStations():LiveData<List<Station>>{
        return  db.generalDao().getLocalStations()
    }


    //business details
    fun apiFetchBusinessDetails(application: Application, id:String){
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.getBusnessDetails(application.getToken(),id)
                if (response.code() == 200){
                    val businessDetails = response.body()
                    businessDetails?.let {
                        db.generalDao().nukeBusinessDetails()
                        db.generalDao().saveBusinessDetails(it)

                    }
                }
            }catch (e:IOException){

            }
        }
    }
    fun localGetBusinessDetails():LiveData<BusinessDetails>{
        Log.d("business_details", "saved")
        return db.generalDao().getLocalBusinessDetails()
    }


    //categories
    fun apiCategories(application: Application) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchCategories(application.getToken())
                if (response.code() == 200) {
                    val category = response.body()
                    category?.let {
                        db.generalDao().nukeCategories()
                        db.generalDao().saveCategoriesLocal(it.categories)
                    }
                }
            } catch (e: IOException) {
                //errors e.g. no internet
            }
        }

    }

    fun localCategories(): LiveData<List<Category>> {
        return db.generalDao().getLocalCategories()
    }

    //products
    fun apiProducts(application: Application) {
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchProducts(application.getToken())
                if (response.code() == 200) {
                    val variation = response.body()
                    variation?.let {
                        db.generalDao().nukeVariations()
                        db.generalDao().saveVariationsLocal(it.variations)
                    }
                }
            } catch (e: IOException) {
                //errors e.g. no internet
            }
        }

    }

    //Variations
    fun localVariations(): LiveData<List<Variations>> {
        return db.generalDao().getLocalVariations()
    }

    fun addTicketItems(ticket: Ticket){
        GlobalScope.launch {
            val item = db.generalDao().checkItemExists(ticket.id)
            if (item != null) {
                val newQty = item.quantity + ticket.quantity
                val newPrice = item.default_sell_price.toDouble() * newQty
                db.generalDao().updateItem(quantity = newQty, pid = item.id)
                db.generalDao().updateItemPrice(price = newPrice, pid = item.id)
            }else{
                db.generalDao().addItem(ticket)
            }
        }
    }
    //Ticket
    fun roomDbItems(ticket: Ticket, context: Context) {
        GlobalScope.launch {
            val item = db.generalDao().checkItemExists(ticket.id)
            if (item != null) {
                val assets : MutableList<String> = arrayListOf()
                assets.addAll(item.assets)
                for (i in ticket.assets) {
                    if (!assets.contains(i)){
                        assets.add(i)
                        val newQty = item.quantity + 1
                        val newPrice = item.default_sell_price.toDouble() * newQty
                        db.generalDao().updateItem(quantity = newQty, pid = item.id)
                        db.generalDao().updateAsset(asset = fromListAssetsToString(assets),pid = item.id)
                        db.generalDao().updateItemPrice(price = newPrice, pid = item.id)
//                        context.showToast("$i added successfully")
                    }else{
//                        context.showToast("$i already added")
                    }
                }

            } else {
                db.generalDao().addItem(ticket)
                Log.d("add-items", "added new item\n$ticket")
            }
        }
    }

    private fun fromListAssetsToString(assets: MutableList<String>): String {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.toJson(assets, type)
    }

    fun addAsset(ticket: Ticket, serial : String, qty : Int, price : Double, context : Context) {
        GlobalScope.launch {
            val assets : MutableList<String> = arrayListOf()
            assets.addAll(ticket.assets)
            if (!assets.contains(serial)){
                assets.add(serial)
                db.generalDao().updateAsset(asset = fromListAssetsToString(assets), pid = ticket.id)
                db.generalDao().updateItem(quantity = qty, pid = ticket.id)
                db.generalDao().updateItemPrice(price = price, pid = ticket.id)
//                context.showToast("$serial added successfully")
            }else{
//                context.showToast("$serial already added")
            }
        }
    }

    fun removeAsset(ticket: Ticket, serial : String, qty : Int, price : Double, context: Context){
        GlobalScope.launch {
            val assets : MutableList<String> = arrayListOf()
            assets.addAll(ticket.assets)
            if (assets.contains(serial)){
                assets.remove(serial)
                db.generalDao().updateAsset(asset = fromListAssetsToString(assets), pid = ticket.id)
                db.generalDao().updateItem(quantity = qty, pid = ticket.id)
                db.generalDao().updateItemPrice(price = price, pid = ticket.id)
//                context.showToast("$serial removed successfully")
            }else{
//                context.showToast("$serial already removed")
            }
        }
    }

    fun updateqty(ticket: Ticket, q: Int) {
        GlobalScope.launch {
            db.generalDao().updateItem(quantity = q, pid = ticket.id)
            Log.d("updated-items", "added $q")
        }
    }

    fun updatePrice(ticket: Ticket, p: Double) {
        GlobalScope.launch {
            db.generalDao().updateItemPrice(price = p, pid = ticket.id)
            Log.d("updated-items", "added $p")
        }
    }

    fun deleteTicket(ticket: Ticket) {
        GlobalScope.launch {
            db.generalDao().deleteItem(ticket.id)
        }
    }

    fun deleteTickets() {
        GlobalScope.launch {
            db.generalDao().nukeTickets()
        }
    }

    fun localItems(): LiveData<List<Ticket>> {
        return db.generalDao().getAllSales()
    }

    // getAssets
    fun getAssetsApi(application: Application){
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.fetchAssets(application.getToken())
                if (response.isSuccessful){
                    val result = response.body()
                    result?.let {
                        db.generalDao().nukeAssets()
                        db.generalDao().saveAssets(it.assets)
                    }
                }else{
                    //failed
                    Log.d("Failed : Assets" , "${response.code()}")
                }
            }catch (e:IOException){
                Log.d("Error : Assets" , "${e.message}")
            }
        }
    }

    fun getLocalAssets() : LiveData<List<Asset>>{
        return db.generalDao().getLocalAssets()
    }



    // make payment
    fun makePayment (invoices: Invoices, application: Application) {
        GlobalScope.launch {
            try {
            val response = ApiClient.webService.makePayment(application.getToken(),"application/json",invoices)
            if (response.isSuccessful){
                val paymentResponse = response.body()!!
            }else{
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(
                    response.errorBody()!!.charStream(),
                    Message::class.java
                )
                val msg = errorResponse.error
                application.showToast(msg)
            }
        }catch (e:IOException){
                Log.d("make-payment", "error : $e")
            }
        }
    }

    //transactions
    fun apiTransactions (application: Application){
        GlobalScope.launch {
            try {
                val response = ApiClient.webService.getTransactions(application.getToken())
                if (response.isSuccessful){
                    val transactions = response.body()
                    transactions?.let {
                        db.generalDao().nukeTransactions()
                        db.generalDao().saveTransactions(it.transactions)
                    }
                }
            }catch (e:IOException){
                Log.d("transactions", "error : $e")
            }
        }
    }
    fun getLocalTransactions() : LiveData<List<Transaction>>{
        return db.generalDao().getLocalTransactions()
    }
}