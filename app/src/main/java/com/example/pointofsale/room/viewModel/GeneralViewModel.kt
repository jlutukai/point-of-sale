package com.example.pointofsale.room.viewModel

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.example.pointofsale.models.*
import com.example.pointofsale.room.repository.GeneralRepository

class GeneralViewModel (application: Application) : AndroidViewModel(application) {
    private val generalRepo = GeneralRepository()

    //login
    fun loginUser(username: String, password: String){
        generalRepo.apiLoginUser(application = getApplication(),username = username, password = password )
    }
    fun saveUserData(username: String, password: String){
        generalRepo.saveUserData(application = getApplication(),username = username, password = password )
    }
    fun getUserData():LiveData<User>{
        return generalRepo.localGetUserData()
    }
    fun getStations():LiveData<List<Station>>{
        return generalRepo.localStations()
    }

    // Business Details
    fun getUpdatedBussinessDetails(id:String){
        generalRepo.apiFetchBusinessDetails(application = getApplication(),id = id)
    }
    fun getLocalBusinessDetails()  : LiveData<BusinessDetails>{
        return generalRepo.localGetBusinessDetails()
    }

    //categories
    fun getLocalCategories(): LiveData<List<Category>> {
        return generalRepo.localCategories()
    }
    fun getUpdatedCategories() {
        generalRepo.apiCategories(application = getApplication())
    }

    //products
    fun getUpdatedProducts() {
        generalRepo.apiProducts(application = getApplication())
    }

    //variations
    fun getLocalVariations(): LiveData<List<Variations>> {
        return generalRepo.localVariations()
    }

    //contact
    fun getLocalContacts():LiveData<List<Contact>>{
        return generalRepo.localContacts()
    }
    fun getUpdatedContacts(){
        generalRepo.apiContacts(application = getApplication())
    }

    //Ticket
    fun roomItems(ticket: Ticket, context: Context){
        generalRepo.roomDbItems(ticket = ticket, context = context )
    }

    fun addItems (ticket: Ticket){
        generalRepo.addTicketItems(ticket = ticket)
    }
    fun getItems() : LiveData<List<Ticket>>{
        return generalRepo.localItems()
    }
    fun deletItem(ticket: Ticket){
        generalRepo.deleteTicket(ticket)
    }
    fun updateQty(ticket: Ticket, q :Int){
        generalRepo.updateqty(ticket,q)
    }
    fun updateItemPrice(ticket: Ticket, p :Double){
        generalRepo.updatePrice(ticket,p)
    }
    fun deleteTickets(){
        generalRepo.deleteTickets()
    }

    fun addAsset(ticket: Ticket, serial : String, q: Int, p: Double, context: Context){
        generalRepo.addAsset(ticket = ticket,serial = serial, qty = q, price = p, context =context)
    }

    fun removeAsset(ticket: Ticket, serial : String, q: Int, p: Double, context: Context){
        generalRepo.removeAsset(ticket = ticket,serial = serial, qty = q, price = p, context = context)
    }

    //transactions
    fun getLocalTransactions() : LiveData<List<Transaction>>{
        return generalRepo.getLocalTransactions()
    }
    fun getUpdatedTransactions(){
        generalRepo.apiTransactions(application = getApplication())
    }

    //assets
    fun getLocalAsstes() : LiveData<List<Asset>> {
      return generalRepo.getLocalAssets()
    }
    fun getUpdatedAssets(){
        return generalRepo.getAssetsApi(application = getApplication())
    }
}