package com.example.pointofsale.room.converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type

class AssetsConverter : Serializable {

    @TypeConverter
    fun fromListAssetsToString(asset: List<String>): String {
        val gson = Gson()
        val type = object : TypeToken<List<String>>() {}.type
        return gson.toJson(asset, type)
    }

    @TypeConverter
    fun fromStringToList(assets: String): List<String> {
        val gson = Gson()
        val type: Type = object : TypeToken<List<String>>() {}.type
        return gson.fromJson(assets, type)
    }
}