package com.example.pointofsale.room

import android.app.Application
import androidx.multidex.MultiDexApplication
import androidx.room.Room
import com.example.pointofsale.utils.AidlUtils
import com.sunmi.pay.hardware.aidlv2.readcard.ReadCardOptV2


class RoomDbInit : Application() {
    private val aidlUtils:AidlUtils = AidlUtils()


    companion object {
        var mReadCardOptV2 : ReadCardOptV2? = null
        lateinit var database: AppDatabase
    }

    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "point_of_sale_db")
//                .allowMainThreadQueries()//to remove after testing
                .fallbackToDestructiveMigration()
                .build()
        aidlUtils.getInstance().connectPrinterService(this)
    }

}