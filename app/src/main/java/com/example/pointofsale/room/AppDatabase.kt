package com.example.pointofsale.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.pointofsale.models.*
import com.example.pointofsale.room.converters.*


@Database(
    entities = [Category::class, Product::class, Variations::class,
        Ticket::class, Contact::class, User::class,
        BusinessDetails::class, Station::class, Transaction::class, ProductVariationAdd::class,
        Asset::class
    ], version = 10, exportSchema = false
)
@TypeConverters(
    ProductConverter::class, ContactConverter::class,
    PaymentLineConverter::class, SellLineConverter::class, AssetsConverter::class
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun generalDao(): GeneralDao
}