package com.example.pointofsale.room.converters

import androidx.room.TypeConverter
import com.example.pointofsale.models.*
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type

class ProductConverter : Serializable {

        @TypeConverter
    fun fromProductToString(product: Product): String {
        val gson = Gson()
        val type = object : TypeToken<Product?>() {}.type
        return gson.toJson(product, type)
    }

    @TypeConverter
    fun fromStringToProduct(product: String): Product {
        val gson = Gson()
        val type: Type = object : TypeToken<Product?>() {}.type
        return gson.fromJson(product, type)
    }
}