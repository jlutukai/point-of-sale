package com.example.pointofsale.room

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.pointofsale.models.*

@Dao
interface GeneralDao {

    //Contacts
    @Insert(onConflict = REPLACE)
    fun saveContactLocal(contact : List<Contact>)
    @Insert(onConflict = REPLACE)
    fun saveContact(contact : Contact)
    @Query("delete from contacts")
    fun nukeContacts()

    @Query("select * from contacts")
    fun getLocalContacts() : LiveData<List<Contact>>

    //categories
    @Insert(onConflict = REPLACE)
    fun saveCategoriesLocal(category: List<Category>)

    @Query("delete from categories")
    fun nukeCategories()

    @Query("select * from categories")
    fun getLocalCategories(): LiveData<List<Category>>


    //Variations
    @Insert(onConflict = REPLACE)
    fun saveVariationsLocal(product: List<Variations>)

    @Query("delete from variations")
    fun nukeVariations()

    @Query("select * from variations")
    fun getLocalVariations(): LiveData<List<Variations>>

    //Ticket
    @Insert (onConflict = REPLACE)
    fun addItem(ticket: Ticket)

    @Query("UPDATE ticket SET quantity = :quantity WHERE id = :pid")
    fun updateItem(quantity: Int, pid: Int): Int

    @Query("UPDATE ticket SET assets = :asset WHERE id = :pid")
    fun updateAsset(asset: String, pid: Int)

    @Query("UPDATE ticket SET sell_price = :price WHERE id = :pid")
    fun updateItemPrice(price: Double, pid: Int): Int

    @Query("delete from ticket where id = :id")
    fun deleteItem(id: Int)

    @Query("delete from ticket")
    fun nukeTickets()

    @Query("select * from ticket where id = :id")
    fun checkItemExists(id:Int): Ticket?

    @Query("select * from ticket")
    fun getAllSales():LiveData<List<Ticket>>



    //user data
    @Insert(onConflict = REPLACE)
    fun saveUserData(user : User)

    @Query("delete from user_data")
    fun nukeUserData()

    @Query("select * from user_data")
    fun getUserData() : LiveData<User>

    //business details
    @Insert(onConflict = REPLACE)
    fun saveBusinessDetails(businessDetails : BusinessDetails)

    @Query("delete from business_details")
    fun nukeBusinessDetails()

    @Query("select * from business_details")
    fun getLocalBusinessDetails() : LiveData<BusinessDetails>

    // Create Invoice

    //stations
    @Insert(onConflict = REPLACE)
    fun saveStations(station: List<Station>)

    @Query("delete from stations")
    fun nukeStattions()

    @Query("select * from stations")
    fun getLocalStations() : LiveData<List<Station>>

    //transactions
    @Insert(onConflict = REPLACE)
    fun saveTransactions(transaction: List<Transaction>)

    @Query("delete from transactions")
    fun nukeTransactions()

    @Query("select * from transactions")
    fun getLocalTransactions() : LiveData<List<Transaction>>

    //assets
    //business details
    @Insert(onConflict = REPLACE)
    fun saveAssets(asset : List<Asset>)

    @Query("delete from asset")
    fun nukeAssets()

    @Query("select * from asset")
    fun getLocalAssets() : LiveData<List<Asset>>

}