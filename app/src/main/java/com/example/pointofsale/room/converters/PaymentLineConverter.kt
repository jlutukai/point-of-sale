package com.example.pointofsale.room.converters

import androidx.room.TypeConverter
import com.example.pointofsale.models.PaymentLine
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.Serializable
import java.lang.reflect.Type

class PaymentLineConverter :Serializable{
    @TypeConverter
    fun fromListToStringP(paymentLine : List<PaymentLine>): String {
        val gson = Gson()
        val type = object : TypeToken<List<PaymentLine?>?>() {}.type
        return gson.toJson(paymentLine, type)
    }

    @TypeConverter
    fun fromStringToListP(paymentLine: String): List<PaymentLine> {
        val gson = Gson()
        val type: Type = object : TypeToken<List<PaymentLine?>?>() {}.type
        return gson.fromJson(paymentLine, type)
    }
}