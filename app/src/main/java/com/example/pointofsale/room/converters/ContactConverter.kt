package com.example.pointofsale.room.converters

import androidx.room.TypeConverter
import com.example.pointofsale.models.Contact
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.io.Serializable

class ContactConverter : Serializable {

    @TypeConverter
    fun fromContactToString(contact: Contact?): String? {
        val gson = Gson()
        val type = object : TypeToken<Contact?>() {}.type
        return gson.toJson(contact, type)
    }

    @TypeConverter
    fun fromStringToContact(contact: String?): Contact? {
        val gson = Gson()
        val type: Type = object : TypeToken<Contact?>() {}.type
        return gson.fromJson(contact, type)
    }
}