package com.example.pointofsale.fragments


import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.example.pointofsale.R
import com.example.pointofsale.adapters.ManageCategoriesAdapter
import com.example.pointofsale.models.AddCategory
import com.example.pointofsale.models.ManageCategory
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.*
import com.github.nikartm.button.FitButton
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException


/**
 * A simple [Fragment] subclass.
 */
class CategoriesFragment : Fragment() {
    private var categories : MutableList<ManageCategory> = arrayListOf()
    private lateinit var loading : Dialog
    private lateinit var generalViewModel: GeneralViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_categories, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null){
            generalViewModel = ViewModelProvider(activity!!).get(GeneralViewModel::class.java)
            loading = Dialog(context!!)
            loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loading.setCancelable(false)
            loading.setContentView(R.layout.loading_dialog)
            loading.show()
        rv_categories.layoutManager = GridLayoutManager(context!!, 2)
        rv_categories.addItemDecoration(SpacingItemDecoration(2, dpToPx(context!!, 5), true))
        rv_categories.setHasFixedSize(true)
        rv_categories.isNestedScrollingEnabled = false
        getData()
            PushDownAnim.setPushDownAnimTo(add_category).setScale(PushDownAnim.MODE_STATIC_DP,8F).setOnClickListener {
                showDialogAddCategory(null)
            }


    }
    }

    private fun showDialogAddCategory(pid : Int?) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_add_stuff)
        val title = dialog.findViewById<TextView>(R.id.title_dialog)
        title.text = "Add New Category"
        val name = dialog.findViewById<EditText>(R.id.name_stuff)
        name.visibility = View.VISIBLE
        val code = dialog.findViewById<EditText>(R.id.short_code)
        code.visibility = View.VISIBLE
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val submit = dialog.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            val categoryName = name.text.toString().trim()
            val categoryCode = code.text.toString().trim()
            if (categoryName.isEmpty()){
                name.error = "Required"
                return@setOnClickListener
            }
            val addCategory = AddCategory(categoryName,categoryCode, pid)
            if (isNetworkConnected(context!!)){
                loading.show()
                dialog.dismiss()
                GlobalScope.launch(Dispatchers.Main) { addNewCategory(addCategory) }
            }else{
                context!!.showToast("Check Internet Connection...")
            }
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            WRAP_CONTENT
        )
    }

    private suspend fun addNewCategory(addCategory: AddCategory) {
        try {
            val response = ApiClient.webService.addCategories(context!!.getToken(), addCategory)
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    context!!.showToast(it.message)
                    getData()
                }
            }else{
                context!!.showToast("Failed : ${response.code()}")
                loading.dismiss()
            }
        }catch (e:IOException){
            context!!.showToast("Error : ${e.message}")
            loading.dismiss()
        }
    }

    private fun getData() {
        if (!isNetworkConnected(context!!)){
            context!!.showToast("Check Internet Connection...")
            loading.dismiss()
        }
        GlobalScope.launch(Dispatchers.Main) { getCategories() }
    }

    private suspend fun getCategories() {
        try {
            val response = ApiClient.webService.getManageCategories(context!!.getToken())
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    categories.clear()
                    categories.addAll(it.categories)
                    val categoryAdapter = ManageCategoriesAdapter(context!!, categories)
                    rv_categories.adapter = categoryAdapter
                    categoryAdapter.onItemClick = {category ->
                        showOptionsDialog(category)
                    }
                    loading.dismiss()
                }
            }else{
                loading.dismiss()
                context!!.showToast("Failed : ${response.code()}")
            }
        }catch (e:IOException){
            loading.dismiss()
            context!!.showToast("Error : ${e.message}")
        }
    }

    private fun showOptionsDialog(category: ManageCategory) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.options_dialog)
        val edit = dialog.findViewById<TextView>(R.id.edit_dialog_options)
        PushDownAnim.setPushDownAnimTo(edit).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            dialog.dismiss()
            showEditDialog(category)
        }
        val delete = dialog.findViewById<TextView>(R.id.delete_dialog_options)
        PushDownAnim.setPushDownAnimTo(delete).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            dialog.dismiss()
            loading.show()
            GlobalScope.launch(Dispatchers.Main) { deleteCategory(category) }
        }
        val close = dialog.findViewById<ImageView>(R.id.close_dialog_options)
        PushDownAnim.setPushDownAnimTo(close).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            WRAP_CONTENT
        )
    }

    private suspend fun deleteCategory(category: ManageCategory) {
        try {
            val response = ApiClient.webService.deleteCategory(context!!.getToken(), category.id)
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    loading.dismiss()
                    context!!.showToast(it.message)
                    getData()
                }
            }else{
                loading.dismiss()
                context!!.showToast("Failed : ${response.code()}")
            }
        }catch (e: IOException){
            loading.dismiss()
            context!!.showToast("Failed : ${e.message}")
        }
    }

    private fun showEditDialog(category: ManageCategory) {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_add_stuff)
        val title = dialog.findViewById<TextView>(R.id.title_dialog)
        title.text = "Edit Category"
        val name = dialog.findViewById<EditText>(R.id.name_stuff)
        name.setText(category.name)
        name.visibility = View.VISIBLE
        val code = dialog.findViewById<EditText>(R.id.short_code)
        code.setText(category.short_code)
        code.visibility = View.VISIBLE
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val submit = dialog.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            val categoryName = name.text.toString().trim()
            val categoryCode = code.text.toString().trim()
            if (categoryName.isEmpty()){
                name.error = "Required"
                return@setOnClickListener
            }
            val addCategory = AddCategory(categoryName,categoryCode, category.parent_id)
            if (isNetworkConnected(context!!)){
                loading.show()
                dialog.dismiss()
                GlobalScope.launch(Dispatchers.Main) { editCategory(addCategory) }
            }else{
                context!!.showToast("Check Internet Connection...")
            }
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            WRAP_CONTENT
        )
    }

    private suspend  fun editCategory(addCategory: AddCategory) {
        try {
            val response = ApiClient.webService.editCategories(context!!.getToken(), addCategory)
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    loading.dismiss()
                    context!!.showToast(it.message)
                    getData()
                }
            }else{
                loading.dismiss()
                context!!.showToast("Failed : ${response.code()}")
            }
        }catch (e: IOException){
            loading.dismiss()
            context!!.showToast("Error : ${e.message}")
        }
    }


}
