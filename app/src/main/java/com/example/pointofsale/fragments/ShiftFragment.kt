package com.example.pointofsale.fragments

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.activities.EndShift
import com.example.pointofsale.adapters.ShiftsAdapter
import com.example.pointofsale.models.Shift
import com.example.pointofsale.models.User
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.showToast
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class ShiftFragment : Fragment() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var user: User
    private lateinit var loading: Dialog
    private lateinit var shiftsAdapter: ShiftsAdapter
    private lateinit var rvShifts: RecyclerView
    private var allShifts: MutableList<Shift> = arrayListOf()
    private var allCashiers: MutableList<User> = arrayListOf()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_shift, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null) {
            rvShifts = view.findViewById(R.id.rv_shifts)
            generalViewModel = ViewModelProvider(activity!!).get(GeneralViewModel::class.java)
            loading = Dialog(context!!)
            loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loading.setCancelable(false)
            loading.setContentView(R.layout.loading_dialog)
            loading.show()
            rvShifts.layoutManager = LinearLayoutManager(requireActivity())
            rvShifts.setHasFixedSize(true)
            rvShifts.isNestedScrollingEnabled = false
            shiftsAdapter = ShiftsAdapter(context!!, allShifts)
            getData()
            setData()
        }

    }

    private fun setData() {

    }


    private fun getData() {
        generalViewModel.getUserData().observe(activity!!, Observer {
            user = it
        })
        GlobalScope.launch(Dispatchers.Main) { getShifts() }
    }

    private suspend fun getShifts() {
        try {
            val response = ApiClient.webService.getCashiers(context!!.getToken())
            if (response.isSuccessful) {
                allCashiers.addAll(response.body()!!.cashiers)
            }
        } catch (e: IOException) {
            //error
        }
        try {
            val response = ApiClient.webService.getShifts(context!!.getToken())
            if (response.isSuccessful) {
                allShifts.addAll(response.body()!!.shifts)

            }
        } catch (e: IOException) {
            loading.dismiss()
            context!!.showToast("Error : ${e.message}")
        }
        allShifts.sortByDescending { it.id }
        shiftsAdapter = ShiftsAdapter(context!!, allShifts)
        rvShifts.adapter = shiftsAdapter
        shiftsAdapter.onItemClick = {
            if (it.status == "open") {
//                context!!.showToast(it.id.toString())
                startActivity(
                    Intent(context!!, EndShift::class.java).putExtra(
                        "id",
                        it.id.toString()
                    )
                )
            }
        }
        loading.dismiss()

    }
}