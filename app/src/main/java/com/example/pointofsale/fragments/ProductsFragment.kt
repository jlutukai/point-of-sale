package com.example.pointofsale.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager

import com.example.pointofsale.R
import com.example.pointofsale.activities.AddProduct
import com.example.pointofsale.adapters.ProductManagementAdapter
import com.example.pointofsale.models.ManageProduct
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.utils.SpacingItemDecoration
import com.example.pointofsale.utils.dpToPx
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.fragment_products.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException


class ProductsFragment : Fragment() {
    private var products : MutableList<ManageProduct> = arrayListOf()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null) {
            rv_manage_products.layoutManager = GridLayoutManager(context!!, 2)
            rv_manage_products.addItemDecoration(
                SpacingItemDecoration(
                    2,
                    dpToPx(context!!, 5),
                    true
                )
            )
            rv_manage_products.setHasFixedSize(true)
            rv_manage_products.isNestedScrollingEnabled = false
            getData()
            PushDownAnim.setPushDownAnimTo(add_product).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                startActivity(Intent(context!!, AddProduct::class.java))
            }
        }
    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getProducts() }
    }

    private suspend fun getProducts() {
        try {
            val response = ApiClient.webService.getManageProducts(context!!.getToken())
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    products.clear()
                    products.addAll(it.products)
                    val productAdapter = ProductManagementAdapter(context!!, products)
                    rv_manage_products.adapter = productAdapter
                    productAdapter.onItemClick = {

                    }
                }
            }
        }catch (e:IOException){
            context!!.showToast("Error : ${e.message}")
        }
    }

}
