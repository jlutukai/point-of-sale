package com.example.pointofsale.fragments


import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.pointofsale.R
import com.example.pointofsale.adapters.ManageVariationsAdapter
import com.example.pointofsale.adapters.ValueAdapter
import com.example.pointofsale.models.AddBrand
import com.example.pointofsale.models.AddVariation
import com.example.pointofsale.models.ManageVariation
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.utils.*
import com.github.nikartm.button.FitButton
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_products.*
import kotlinx.android.synthetic.main.fragment_variations.*
import kotlinx.android.synthetic.main.item_ticket.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.DisposableHandle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class VariationsFragment : Fragment() {
    private var variations : MutableList<ManageVariation> = arrayListOf()
    val values : MutableList<String> = arrayListOf()
    private lateinit var loading : Dialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_variations, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null){
            loading = Dialog(context!!)
            loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loading.setCancelable(false)
            loading.setContentView(R.layout.loading_dialog)
            loading.show()
        rv_manage_variations.layoutManager = GridLayoutManager(context!!, 2)
        rv_manage_variations.addItemDecoration(SpacingItemDecoration(2, dpToPx(context!!, 5), true))
        rv_manage_variations.setHasFixedSize(true)
        rv_manage_variations.isNestedScrollingEnabled = false
        getData()
            PushDownAnim.setPushDownAnimTo(add_variation).setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                showAddVariation()
            }
        }
    }

    private fun showAddVariation() {
        values.clear()
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_add_stuff)
        val title = dialog.findViewById<TextView>(R.id.title_dialog)
        title.text = "Add New Variation"
        val name = dialog.findViewById<EditText>(R.id.name_stuff)
        name.visibility = View.VISIBLE
        val recyclerView = dialog.findViewById<RecyclerView>(R.id.rv_values)
        recyclerView.visibility = View.VISIBLE
        recyclerView.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        recyclerView.setHasFixedSize(true)
        recyclerView.isNestedScrollingEnabled = true
        val valueAdapter = ValueAdapter(context!!, values)
        recyclerView.adapter = valueAdapter
        valueAdapter.onItemClick = {
            values.remove(it)
            valueAdapter.notifyDataSetChanged()
        }
        val addValue = dialog.findViewById<FitButton>(R.id.add_value)
        addValue.visibility = View.VISIBLE
        addValue.setOnClickListener {
            showValueDialog()
            valueAdapter.notifyDataSetChanged()
        }
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val submit = dialog.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            val variationName = name.text.toString().trim()
            if (variationName.isEmpty()){
                name.error = "Required"
                return@setOnClickListener
            }
            if (values.isEmpty()){
               context!!.showToast("Please add value")
                return@setOnClickListener
            }
            val addVariation = AddVariation(variationName,values)
            if (isNetworkConnected(context!!)){
                loading.show()
                dialog.dismiss()
                GlobalScope.launch(Dispatchers.Main) { addNewVariation(addVariation) }
            }else{
                context!!.showToast("Check Internet Connection...")
            }
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private fun showValueDialog() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_add_stuff)
        val title = dialog.findViewById<TextView>(R.id.title_dialog)
        title.text = "Add New Value"
        val name = dialog.findViewById<EditText>(R.id.name_stuff)
        name.visibility = View.VISIBLE
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val submit = dialog.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            val valueName = name.text.toString().trim()
            if (valueName.isEmpty()){
                name.error = "Required"
                return@setOnClickListener
            }
            values.add(valueName)
            dialog.dismiss()
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private suspend fun addNewVariation(addVariation: AddVariation) {
        try {
            val response = ApiClient.webService.addVaiations(context!!.getToken(), addVariation)
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    context!!.showToast(it.message)
                    getData()
                }
            }else{
                context!!.showToast("Failed : ${response.code()}")
                loading.dismiss()
            }
        }catch (e:IOException){
            loading.dismiss()
            context!!.showToast("Error : ${e.message}")
        }
    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getVariations() }
    }

    private suspend fun getVariations() {
        try {
            val response = ApiClient.webService.getManageVaiations(context!!.getToken())
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    variations.clear()
                    variations.addAll(it.variations)
                    val variationAdapter = ManageVariationsAdapter(context!!, variations)
                    rv_manage_variations.adapter = variationAdapter
                    variationAdapter.onItemClick = {
                    }
                }
                loading.dismiss()
            }else{
                loading.dismiss()
                context!!.showToast("Error : ${response.code()}")
            }
        }catch (e:IOException){
            loading.dismiss()
            context!!.showToast("Error : ${e.message}")
        }
    }
}
