package com.example.pointofsale.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager

import com.example.pointofsale.R
import com.example.pointofsale.adapters.UnitAdapter
import com.example.pointofsale.models.ManageUnit
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.utils.SpacingItemDecoration
import com.example.pointofsale.utils.dpToPx
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.showToast
import kotlinx.android.synthetic.main.fragment_brands.*
import kotlinx.android.synthetic.main.fragment_unit.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class UnitFragment : Fragment() {
    private var units : MutableList<ManageUnit> = arrayListOf()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_unit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null) {
            rv_manage_unit.layoutManager = GridLayoutManager(context!!, 2)
            rv_manage_unit.addItemDecoration(SpacingItemDecoration(2, dpToPx(context!!, 5), true))
            rv_manage_unit.setHasFixedSize(true)
            rv_manage_unit.isNestedScrollingEnabled = false
            getData()
        }
    }
//    4139004

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getUnits() }
    }

    private suspend fun getUnits() {
        try {
            val response = ApiClient.webService.getManageUnits(context!!.getToken())
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    units.clear()
                    units.addAll(it.units)
                    val unitAdapter = UnitAdapter(context!!, units)
                    rv_manage_unit.adapter = unitAdapter
                    unitAdapter.onItemClick = {

                    }
                }
            }else{
                context!!.showToast("Failed : ${response.code()}")
            }
        }catch (e:IOException){
            context!!.showToast("Error : ${e.message}")
        }
    }

}
