package com.example.pointofsale.fragments

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pointofsale.R
import com.example.pointofsale.activities.TransactionDetails
import com.example.pointofsale.adapters.TransactionAdapter
import com.example.pointofsale.models.Transaction
import com.example.pointofsale.models.User
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.isNetworkConnected
import com.example.pointofsale.utils.showToast
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_transactions.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException
import java.util.*

class TransactionsFragment : Fragment() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var dialog: Dialog
    private lateinit var loading: Dialog
    private lateinit var user: User
    private var allTransactions: MutableList<Transaction> = arrayListOf()
    val filtered: MutableList<Transaction> = arrayListOf()
    private var allCashiers: MutableList<User> = arrayListOf()
    private var cashiers: MutableList<String> = arrayListOf()
    private var st: String = ""
    private var ca: String = ""
    private var da: String = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        generalViewModel = ViewModelProvider(activity!!).get(GeneralViewModel::class.java)
        return inflater.inflate(R.layout.fragment_transactions, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null) {
            loading = Dialog(context!!)
            loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loading.setCancelable(false)
            loading.setContentView(R.layout.loading_dialog)
            loading.show()
            getData()
            rv_transactions.layoutManager = LinearLayoutManager(context!!)
            PushDownAnim.setPushDownAnimTo(filter).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                .setOnClickListener {
                    val status: List<String> = listOf("All", "partial", "paid", "due")
                    dialog = Dialog(context!!)
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                    dialog.setCancelable(true)
                    dialog.setContentView(R.layout.filter_dialog)
                    val spinnerch = dialog.findViewById<Spinner>(R.id.cashiers)
                    val adapter: ArrayAdapter<String> = ArrayAdapter(
                        context!!,
                        android.R.layout.simple_spinner_item,
                        cashiers as List<String>
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinnerch.adapter = adapter
                    spinnerch.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(p0: AdapterView<*>?) {
                            }

                            override fun onItemSelected(
                                p0: AdapterView<*>?,
                                p1: View?,
                                p2: Int,
                                p3: Long
                            ) {
                                ca = p0!!.getItemAtPosition(p2).toString()
                            }
                        }
                    val spinnerst = dialog.findViewById<Spinner>(R.id.status)
                    val adapter1: ArrayAdapter<String> = ArrayAdapter(
                        context!!,
                        android.R.layout.simple_spinner_item,
                        status
                    )
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    spinnerst.adapter = adapter1
                    spinnerst.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(p0: AdapterView<*>?) {
                            }

                            override fun onItemSelected(
                                p0: AdapterView<*>?,
                                p1: View?,
                                p2: Int,
                                p3: Long
                            ) {
                                st = p0!!.getItemAtPosition(p2).toString()
                            }
                        }
                    val dateE = dialog.findViewById<EditText>(R.id.date)
                    dateE.setOnClickListener {
                        val c = Calendar.getInstance()
                        val year = c.get(Calendar.YEAR)
                        val month = c.get(Calendar.MONTH)
                        val day = c.get(Calendar.DAY_OF_MONTH)
                        val dpd = DatePickerDialog(
                            activity!!,
                            DatePickerDialog.OnDateSetListener { view, yr, monthOfYear, dayOfMonth ->
                                da = "$yr-${monthOfYear.plus(1)}-$dayOfMonth"
                                dateE.setText(da)
                            },
                            year,
                            month,
                            day
                        )

                        dpd.show()
                    }
                    val cancel = dialog.findViewById<TextView>(R.id.cancel)
                    PushDownAnim.setPushDownAnimTo(cancel).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                        .setOnClickListener {
                            dialog.dismiss()
                        }
                    val change = dialog.findViewById<TextView>(R.id.apply)
                    PushDownAnim.setPushDownAnimTo(change).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                        .setOnClickListener {
                            applyFilter()
                        }
                    dialog.show()
                    val window = dialog.window
                    window!!.setLayout(
                        ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                }
            if (allTransactions.size == 0) {
                getData()
            }
        }
    }

    private fun applyFilter() {
        var uid = 0
        for (i in allCashiers) {
            if (i.first_name + " " + i.last_name == ca && ca != "All") {
                uid = i.id
            }
        }
        filtered.clear()
        for (i in filtered) {

            if (i.transaction_date.substring(
                    0,
                    10
                ) == da || i.payment_status == st || i.created_by == uid
            ) {
                filtered.add(i)
            }

        }
        context!!.showToast(filtered.size.toString())
        filtered.sortByDescending { it.id }
        val adapter = TransactionAdapter(context!!, filtered)
        rv_transactions.adapter = adapter
        adapter.onItemClick = {
            startActivity(
                Intent(context!!, TransactionDetails::class.java).putExtra(
                    "id",
                    it.id.toString()
                )
            )
        }
        dialog.dismiss()

    }

    private fun getData() {
        generalViewModel.getUserData().observe(activity!!, Observer {
            user = it
        })
        if (isNetworkConnected(context!!)) {
            generalViewModel.getUpdatedTransactions()
        }
        generalViewModel.getLocalTransactions().observe(activity!!, Observer { transactions ->
            filtered.clear()
            filtered.addAll(transactions)
            allTransactions.addAll(transactions)
            if (context != null) {
                filtered.sortByDescending { it.id }
                val adapter = TransactionAdapter(context!!, filtered)
                rv_transactions.adapter = adapter
                adapter.onItemClick = {
                    startActivity(
                        Intent(context!!, TransactionDetails::class.java).putExtra(
                            "id",
                            it.id.toString()
                        )
                    )
                }
            }
        })
        GlobalScope.launch(Dispatchers.Main) { getTransactions() }
    }

    private suspend fun getTransactions() {
        try {
            val response = ApiClient.webService.getCashiers(context!!.getToken())
            if (response.isSuccessful) {
                val cas = response.body()
                cas?.let {
                    allCashiers.addAll(it.cashiers)
                    cashiers.clear()
                    cashiers.add("All")
                    for (i in it.cashiers) {
                        cashiers.add(i.first_name + " " + i.last_name)
                    }
                }
                loading.dismiss()
            }
            if (response.code() == 426) {
                loading.dismiss()
                context!!.showToast("Server Error : Too many requests ")
            }
        } catch (e: IOException) {
            //error
            loading.dismiss()
        }

    }
}