package com.example.pointofsale.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.pointofsale.R
import com.example.pointofsale.activities.Login
import com.example.pointofsale.utils.clearLoginDetails

class SendFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        clearLoginDetails()
        startActivity(Intent(context!!, Login::class.java))
        activity!!.finish()
        return inflater.inflate(R.layout.fragment_send, container, false)
    }
}