package com.example.pointofsale.fragments


import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager

import com.example.pointofsale.R
import com.example.pointofsale.adapters.BrandAdapter
import com.example.pointofsale.models.AddBrand
import com.example.pointofsale.models.AddCategory
import com.example.pointofsale.models.ManageBrand
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.utils.*
import com.github.nikartm.button.FitButton
import com.google.android.material.textfield.TextInputLayout
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.fragment_brands.*
import kotlinx.android.synthetic.main.fragment_categories.*
import kotlinx.android.synthetic.main.fragment_variations.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

/**
 * A simple [Fragment] subclass.
 */
class BrandsFragment : Fragment() {
    private var brands : MutableList<ManageBrand> = arrayListOf()
    private lateinit var loading : Dialog
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_brands, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null) {
            loading = Dialog(context!!)
            loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
            loading.setCancelable(false)
            loading.setContentView(R.layout.loading_dialog)
            loading.show()
            rv_manage_brands.layoutManager = GridLayoutManager(context!!, 2)
            rv_manage_brands.addItemDecoration(SpacingItemDecoration(2, dpToPx(context!!, 5), true))
            rv_manage_brands.setHasFixedSize(true)
            rv_manage_brands.isNestedScrollingEnabled = false
            getData()
            PushDownAnim.setPushDownAnimTo(add_brand).setScale(PushDownAnim.MODE_STATIC_DP,8F).setOnClickListener {
                showDialogAddBrand()
            }
        }
    }

    private fun showDialogAddBrand() {
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_add_stuff)
        val title = dialog.findViewById<TextView>(R.id.title_dialog)
        title.text = "Add New Brand"
        val name = dialog.findViewById<EditText>(R.id.name_stuff)
        name.visibility = View.VISIBLE
        val code = dialog.findViewById<EditText>(R.id.description_stuff)
        code.visibility = View.VISIBLE
        val container = dialog.findViewById<TextInputLayout>(R.id.description_stuff_container)
        container.visibility = View.VISIBLE
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val submit = dialog.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            val brandName = name.text.toString().trim()
            val brandDescription = code.text.toString().trim()
            if (brandName.isEmpty()){
                name.error = "Required"
                return@setOnClickListener
            }
            val addBrand = AddBrand(brandName,brandDescription)
            if (isNetworkConnected(context!!)){
                loading.show()
                dialog.dismiss()
                GlobalScope.launch(Dispatchers.Main) { addNewBrand(addBrand) }
            }else{
                context!!.showToast("Check Internet Connection...")
            }
        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    private suspend fun addNewBrand(addBrand: AddBrand) {
        try {
            val response = ApiClient.webService.addBrands(context!!.getToken(), addBrand)
            if (response.isSuccessful){
                val res = response.body()
                res?.let {
                    context!!.showToast(it.message)
                    getData()
                }
            }else{
                context!!.showToast("Failed : ${response.code()}")
                loading.dismiss()
            }
        }catch (e:IOException){
            context!!.showToast("Error : ${e.message}")
            loading.dismiss()
        }

    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getBrands() }
    }

    private suspend fun getBrands() {
        try {
            val response = ApiClient.webService.getManageBrands(context!!.getToken())
            if (response.isSuccessful){
                val res = response.body()
                res?.let{
                    brands.clear()
                    brands.addAll(it.brands)
                    val brandsAdapter = BrandAdapter(context!!, brands)
                    rv_manage_brands.adapter = brandsAdapter
                    brandsAdapter.onItemClick = {
                    }
                    loading.dismiss()
                }
            }else{
                context!!.showToast("Failed : ${response.code()}")
                loading.dismiss()
            }
        }catch (e:IOException){
            context!!.showToast("Error : ${e.message}")
            loading.dismiss()
        }
    }
}
