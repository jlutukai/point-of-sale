package com.example.pointofsale.fragments

import android.animation.Animator
import android.app.Dialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.pointofsale.R
import com.example.pointofsale.activities.AddNewContact
import com.example.pointofsale.activities.BarcodeScanner
import com.example.pointofsale.activities.Cart
import com.example.pointofsale.activities.OpenRegister
import com.example.pointofsale.adapters.ProductsAdapter
import com.example.pointofsale.models.Contact
import com.example.pointofsale.models.Shift
import com.example.pointofsale.models.Ticket
import com.example.pointofsale.models.Variations
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.RoomDbInit
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.*
import com.github.nikartm.button.FitButton
import com.squareup.picasso.Picasso
import com.sunmi.pay.hardware.aidl.AidlConstants
import com.sunmi.pay.hardware.aidlv2.readcard.CheckCardCallbackV2
import com.thekhaeng.pushdownanim.PushDownAnim
import com.toptoche.searchablespinnerlibrary.SearchableSpinner
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.item_product.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fabiomsr.moneytextview.MoneyTextView
import java.io.IOException
import java.util.*

class HomeFragment : Fragment() {
    private var nfcAdapter: NfcAdapter? = null
    private var showDialog : Boolean = true
    private lateinit var pendingIntent: PendingIntent
    private lateinit var generalViewModel: GeneralViewModel
    private var productsAdapter: ProductsAdapter? = null
    private lateinit var rvProducts: RecyclerView
    private var cust_id: String? = null
    private var serial: String = ""
    private var allShifts: MutableList<Shift> = arrayListOf()
    private var openShift = 0
    private lateinit var walkinid: String
    private var allProducts: MutableList<Variations> = arrayListOf()
    private var allContacts: MutableList<Contact> = arrayListOf()
    private var allTickets: MutableList<Ticket> = arrayListOf()
    private var allAssets: MutableList<String> = arrayListOf()
    var searchList: MutableList<Variations> = arrayListOf()
    private lateinit var cartManenos: LinearLayout
    private lateinit var countTxt: TextView
    private lateinit var totalTxt: TextView
    private lateinit var mHandler: Handler
    private lateinit var handler : Handler
    private lateinit var sn: EditText
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_home, container, false)
        generalViewModel = ViewModelProvider(activity!!).get(GeneralViewModel::class.java)

        if (context != null) {
//            setupView()
            getCategories()
            rvProducts = v.findViewById(R.id.rv_products)
            cartManenos = v.findViewById(R.id.cart_manenos)
            countTxt = v.findViewById(R.id.count)
            totalTxt = v.findViewById(R.id.totals)
            rvProducts.layoutManager = GridLayoutManager(context!!, 3)
            rvProducts.addItemDecoration(SpacingItemDecoration(3, dpToPx(context!!, 8), true))
            rvProducts.setHasFixedSize(true)
            rvProducts.isNestedScrollingEnabled = false
            productsAdapter = ProductsAdapter(activity!!, allProducts)
            getProducts()
            showTicketItems()
            getContactsData()
            PushDownAnim.setPushDownAnimTo(v.ticket).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                .setOnClickListener {
                    if (context!!.getStationId() == "null" || allShifts.size == 0) {
                        startActivity(Intent(context!!, OpenRegister::class.java))
                    } else {
                        showTicketItems()
                        openDialog()
                    }
//                    v.expandable_layout.isExpanded = !v.expandable_layout.isExpanded
                }
            PushDownAnim.setPushDownAnimTo(v.add_customer).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                .setOnClickListener {

                    addCustomerToBill()

                }
            PushDownAnim.setPushDownAnimTo(v.barcode).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                .setOnClickListener {
                    startActivity(Intent(context!!, BarcodeScanner::class.java))
                }


        }

        val search = v.findViewById<androidx.appcompat.widget.SearchView>(R.id.search)
        search.setOnQueryTextListener(object :
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                search.hideKeyboard()
                return true
            }

            override fun onQueryTextChange(p0: String?): Boolean {
                if (p0!!.isNotEmpty()) {
                    searchList.clear()
                    val char = p0.toLowerCase(Locale.getDefault())
                    for (i in allProducts) {
                        if (i.name.toLowerCase(Locale.getDefault()).contains(char)) {
                            searchList.add(i)
                        }
                    }
                    val adapter = ProductsAdapter(requireActivity(), searchList)
                    rvProducts.adapter = adapter
                    adapter.onItemClick = {
                        getProductVariations(it)
                    }
                    if (!showDialog){
                        adapter.setActionListener(object : ProductsAdapter.ProductItemActionListener {
                            override fun onItemTap(imageView: View?) {
                                if (imageView != null)
                                    makeFlyAnimation(imageView)
                            }

                        })}
                } else {
                    searchList.clear()
                    searchList.addAll(allProducts)
                    val adapter = ProductsAdapter(requireActivity(), searchList)
                    rvProducts.adapter = adapter
                    if (!showDialog){
                    adapter.setActionListener(object : ProductsAdapter.ProductItemActionListener {
                        override fun onItemTap(imageView: View?) {
                            if (imageView != null)
                                makeFlyAnimation(imageView)
                        }

                    })}
                    adapter.onItemClick = {
                        getProductVariations(it)
                    }
                }
                return true
            }

        })

        if (allProducts.size == 0) {
            getProducts()
        }
        return v
    }

//    private fun setupView() {
//        owl_bottom_sheet.setActivityView((this.activity as AppCompatActivity?)!!)
//
//        //icon to show in collapsed sheet
//        owl_bottom_sheet.setIcon(R.drawable.ic_keyboard_arrow_down_black_24dp)
//
//        //bottom sheet color
//        owl_bottom_sheet.setBottomSheetColor(ContextCompat.getColor(context!!,R.color.colorPrimaryDark))
//
//        //view shown in bottom sheet
//        owl_bottom_sheet.attachContentView(R.layout.cart_dialog)
//
//        //getting close button from view shown
//        owl_bottom_sheet.contentView.findViewById<ImageView>(R.id.close_cart)
//                .setOnClickListener{
//                    owl_bottom_sheet.collapse()
//                }
//    }

    private fun makeFlyAnimation(imageView: View) {
        CircleAnimationUtil().attachActivity(activity).setTargetView(imageView)
            .setMoveDuration(300).setDestView(count_layout)
            .setAnimationListener(object : Animator.AnimatorListener {
                override fun onAnimationRepeat(animation: Animator?) {

                }

                override fun onAnimationEnd(animation: Animator?) {
                    showTicketItems()
                }

                override fun onAnimationCancel(animation: Animator?) {

                }

                override fun onAnimationStart(animation: Animator?) {

                }
            }).startAnimation()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (context != null) {
            nfcAdapter = NfcAdapter.getDefaultAdapter(context!!)
            if (nfcAdapter == null) {
                context!!.showToast("No NFC")
                return
            }
            pendingIntent = PendingIntent.getActivity(
                context!!,
                0,
                Intent(context!!, context!!.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
                0
            )
        }

    }

    private fun resolveIntent(intent: Intent?) {
        val action = intent!!.action
        if (NfcAdapter.ACTION_TAG_DISCOVERED == action || NfcAdapter.ACTION_TECH_DISCOVERED == action || NfcAdapter.ACTION_NDEF_DISCOVERED == action) {
            val id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
            var hexdump = String()
            for (i in id.indices) {
                var x =
                    Integer.toHexString(id[i].toInt() and 0xff)
                if (x.length == 1) {
                    x = "0$x"
                }
                hexdump += "$x "
            }
            serial = hexdump.replace("\\s".toRegex(), "")
        }
    }

    override fun onResume() {
        super.onResume()
        if (nfcAdapter != null) {
            if (!nfcAdapter!!.isEnabled)
                showWirelessSettings()
            nfcAdapter!!.enableForegroundDispatch(activity!!, pendingIntent, null, null)
        }
    }

    private fun showWirelessSettings() {
        context!!.showToast("You need to enable NFC")
        val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
        startActivity(intent)
    }


    private fun getContactsData() {
        if (isNetworkConnected(context!!) && allContacts.isEmpty()) {
            generalViewModel.getUpdatedContacts()
        }
        generalViewModel.getLocalContacts().observe(activity!!, Observer { contacts ->
            allContacts.clear()
            allContacts.addAll(contacts)
            for (i in contacts) {
                if (i.name == "Walk-In Customer") {
                    walkinid = i.id.toString()

                }
            }
//            context!!.showToast(walkinid +" size ${allContacts.size}")
        })
    }

    private fun addCustomerToBill() {
        val contactNames: MutableList<String> = arrayListOf()
        if (activity != null) {
            generalViewModel.getLocalContacts().observe(activity!!, Observer { contacts ->
                allContacts.clear()
                allContacts.addAll(contacts)
                if (contacts.isNotEmpty()) {
                    contactNames.clear()
                    contactNames.add("None")
                    for (i in contacts) {
                        contactNames.add(i.name)
                    }
                    if (context != null) {
                        val dialog = RoundedBottomSheetDialog(context!!)
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                        dialog.setCancelable(true)
                        dialog.setContentView(R.layout.contact_dialog)
                        val add = dialog.findViewById<TextView>(R.id.add_new_customer)
                        PushDownAnim.setPushDownAnimTo(add)
                            .setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                                val intent = Intent(context!!, AddNewContact::class.java)
                                startActivity(intent)
                            }
                        val spinner = dialog.findViewById<SearchableSpinner>(R.id.customers)
                        val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
                            context!!,
                            android.R.layout.simple_spinner_item,
                            contactNames as List<CharSequence>
                        )
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        spinner?.adapter = adapter
                        spinner?.setTitle("Select Customer")
                        spinner?.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                }

                                override fun onItemSelected(
                                    p0: AdapterView<*>?,
                                    p1: View?,
                                    p2: Int,
                                    p3: Long
                                ) {
                                    val selectedItem = p0!!.getItemAtPosition(p2).toString()
                                    if (selectedItem != "None") {
                                        for (i in contacts) {
                                            if (i.name == selectedItem) {
                                                cust_id = i.id.toString()
                                                dialog.dismiss()
                                            }
                                        }
                                        context!!.showToast("Selected $selectedItem successfully")
                                    }
                                }
                            }
                        val close = dialog.findViewById<FitButton>(R.id.close_contact)
                        PushDownAnim.setPushDownAnimTo(close)
                            .setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
                                dialog.dismiss()
                            }
                        dialog.show()
//                        val window = dialog.window
//                        window!!.setLayout(
//                            ViewGroup.LayoutParams.MATCH_PARENT,
//                            ViewGroup.LayoutParams.WRAP_CONTENT
//                        )
                    }
                }
            })
        }

    }

    private fun openDialog() {
//        CartDialog.display(activity!!.supportFragmentManager)
        if (cust_id == null) {
//            addCustomerToBill()
            val intent = Intent(context!!, Cart::class.java)
            intent.putExtra("cid", walkinid)
            startActivity(intent)
        } else {
            val intent = Intent(context!!, Cart::class.java)
            intent.putExtra("cid", cust_id)
            startActivity(intent)
        }
    }

    private fun getCategories() {
        val categoryNames: MutableList<String> = arrayListOf()
        GlobalScope.launch { getShifts() }
        if (isNetworkConnected(context!!) && allTickets.isEmpty()) {
            generalViewModel.getUpdatedCategories()
            generalViewModel.getUpdatedAssets()
        }
        if (activity != null) {
            generalViewModel.getLocalAsstes().observe(activity!!, Observer { a ->
                if (a.isNotEmpty()) {
                    allAssets.clear()
                    a.forEach {
                        allAssets.add(it.serial)
                    }
                }
            })
            generalViewModel.getLocalCategories().observe(activity!!, Observer { categories ->
                if (categories.isNotEmpty()) {
                    categoryNames.clear()
                    categoryNames.add("All Categories")
                    for (i in categories) {
                        categoryNames.add(i.name)
                    }
                    if (context != null && categories_spinner != null) {
                        val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
                            context!!,
                            android.R.layout.simple_spinner_item,
                            categoryNames as List<CharSequence>
                        )
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                        categories_spinner.adapter = adapter
                        categories_spinner.setTitle("Select Category")
                        categories_spinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(p0: AdapterView<*>?) {
                                }

                                override fun onItemSelected(
                                    p0: AdapterView<*>?,
                                    p1: View?,
                                    p2: Int,
                                    p3: Long
                                ) {
                                    val selectedItem = p0!!.getItemAtPosition(p2).toString()
                                    if (selectedItem == "All Categories") {
                                        getProducts()
                                    }
                                    for (i in categories) {
                                        if (i.name == selectedItem) {
                                            getProduct(i.id)
                                        }
                                    }
                                }
                            }
                    } else {
                        if (context != null) {
                            context!!.showToast("No Categories found. Please add categories to continue")
                        }
                    }
                }
            })
        }
    }

    private suspend fun getShifts() {
        try {
            val response = ApiClient.webService.getShifts(context!!.getToken())
            if (response.isSuccessful) {
                allShifts.clear()
                for (i in response.body()!!.shifts) {
                    if (i.status == "open") {
                        allShifts.add(i)
                    }
                }
            }
        } catch (e: IOException) {

        }
    }

    private fun editQuantity(ticket: Ticket){
        handler = Handler(Looper.getMainLooper())
        val mBottomSheetDialog = RoundedBottomSheetDialog(context!!)
        val sheetView = layoutInflater.inflate(R.layout.edit_added_product, null)
        mBottomSheetDialog.setContentView(sheetView)
        val image = mBottomSheetDialog.findViewById<ImageView>(R.id.image_added)
        Picasso.get().load("http://165.90.23.196/uploads/img/${ticket.product.image}").into(image)
        val name = mBottomSheetDialog.findViewById<TextView>(R.id.item_name_added)
        name?.text = ticket.product.name
        val qty = mBottomSheetDialog.findViewById<EditText>(R.id.qty_added)
        qty?.setText(ticket.quantity.toString())
        val cost = mBottomSheetDialog.findViewById<MoneyTextView>(R.id.cost_added)
        cost?.amount = ticket.sell_price.toFloat()
        val delete = mBottomSheetDialog.findViewById<ImageView>(R.id.delete_added)
        delete?.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }
        val submit = mBottomSheetDialog.findViewById<FitButton>(R.id.submit_prod_add)
        submit?.setOnClickListener {
            val newQty = qty?.text.toString()
            if (newQty.isEmpty()){
                qty?.error = "Required"
                return@setOnClickListener
            }
            if (newQty.toInt() < 1){
                qty?.error = "Not Allowed"
                return@setOnClickListener
            }
            ticket.quantity = newQty.toInt()
            ticket.sell_price = ticket.default_sell_price.toDouble() * newQty.toInt()
            if (image != null) makeFlyAnimation(image)
            generalViewModel.addItems(ticket)
            mBottomSheetDialog.dismiss()
        }
        val dismiss = mBottomSheetDialog.findViewById<FitButton>(R.id.close_prod_add)
        dismiss?.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }
        mBottomSheetDialog.show()
//        generalViewModel.addItems(ticket)

    }

    fun View.hideKeyboard() {
        val inputManager =
            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(windowToken, 0)
    }

    private fun getProduct(id: Int) {
        searchList.clear()
        for (i in allProducts) {
            if (i.product.category_id == id) {
                searchList.add(i)
            }
        }
        productsAdapter = ProductsAdapter(context!!, searchList)
        rv_products.adapter = productsAdapter
        if (!showDialog){
        productsAdapter!!.setActionListener(object : ProductsAdapter.ProductItemActionListener {
            override fun onItemTap(imageView: View?) {
                if (imageView != null)
                    makeFlyAnimation(imageView)
            }

        })
        }
        productsAdapter!!.onItemClick = {
            getProductVariations(it)
        }


    }

    private fun getProducts() {
        if (isNetworkConnected(context!!) && allTickets.isEmpty()) {
            generalViewModel.getUpdatedProducts()
        }
//        if (activity != null) {
        generalViewModel.getLocalVariations().observe(activity!!, Observer { variations ->
            allProducts.clear()
            searchList.clear()
            searchList.addAll(variations)
            allProducts.addAll(variations)
            if (context != null) {
                val adapter = ProductsAdapter(context!!, variations)
                rvProducts.adapter = adapter
                if (!showDialog){
                adapter.setActionListener(object : ProductsAdapter.ProductItemActionListener {
                    override fun onItemTap(imageView: View?) {
                        if (imageView != null)
                            makeFlyAnimation(imageView)
//                            adapter.notifyDataSetChanged()
                    }

                })}
                adapter.onItemClick = {
                    getProductVariations(it)
                }
            }
        })


    }

    private fun getProductVariations(variation: Variations) {
        val assets: MutableList<String> = arrayListOf()
        if (variation.product.enable_sr_no == 1) {
            checkCard()
            getSerialDialog(variation)
        }
        if (variation.product.enable_sr_no == 0) {
            if (activity != null) {
                val ticket = Ticket(
                    id = variation.id,
                    name = variation.name,
                    product_id = variation.product_id,
                    default_sell_price = variation.default_sell_price,
                    default_purchase_price = variation.default_purchase_price,
                    dpp_inc_tax = variation.dpp_inc_tax,
                    sell_price_inc_tax = variation.sell_price_inc_tax,
                    product = variation.product,
                    assets = assets,
                    sell_price = variation.default_sell_price.toDouble(),
                    quantity = 1
                )
                if (showDialog){
                    editQuantity(ticket)
                }else{
                    generalViewModel.addItems(ticket)
                }

            }
        }

    }

    private fun checkCard() {
        mHandler = Handler(Looper.getMainLooper())
        Log.d("connection", "checkcard")
        try {
            val cardType: Int = AidlConstants.CardType.MIFARE.value
            RoomDbInit.mReadCardOptV2!!.checkCard(cardType, mCheckCardCallback, 60)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val mCheckCardCallback: CheckCardCallbackV2 = object : CheckCardCallbackV2Wrapper() {

        override fun findRFCard(p0: String?) {
            handleResult(p0)
        }

        override fun onError(p0: Int, p1: String?) {
            if (p1 != null) context!!.showToast("Error $p0 : $p1")
            checkCard()
        }
    }

    private fun handleResult(uuid: String?) {

        mHandler.post {
            Log.d("connection", "result $uuid")
            sn.setText(uuid)
//            if (!isFinishing) {
//                mHandler.postDelayed(::checkCard, 5000)
//            }
        }
    }

    private fun getSerialDialog(variation: Variations) {
        val assets: MutableList<String> = arrayListOf()
        val dialog = Dialog(context!!)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(true)
        dialog.setContentView(R.layout.dialog_add_stuff)
        val title = dialog.findViewById<TextView>(R.id.title_dialog)
        title.text = "Add Asset Tag"
        sn = dialog.findViewById<EditText>(R.id.serials)
        sn.visibility = View.VISIBLE
        val close = dialog.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog.dismiss()
        }
        val submit = dialog.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            val serial = sn.text.toString()
            if (serial.isEmpty()) {
                sn.error = "Add Serial"
                return@setOnClickListener
            }
            if (!allAssets.contains(serial)) {
                context!!.showToast("Asset does not exists")
                return@setOnClickListener
            }
            assets.add(serial)
            if (activity != null) {
                val ticket = Ticket(
                    id = variation.id,
                    name = variation.name,
                    product_id = variation.product_id,
                    default_sell_price = variation.default_sell_price,
                    default_purchase_price = variation.default_purchase_price,
                    dpp_inc_tax = variation.dpp_inc_tax,
                    sell_price_inc_tax = variation.sell_price_inc_tax,
                    product = variation.product,
                    assets = assets,
                    sell_price = variation.default_sell_price.toDouble(),
                    quantity = 1
                )
                generalViewModel.roomItems(ticket, context!!)

            }
            dialog.dismiss()

        }
        dialog.show()
        val window = dialog.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

    }

    private fun showTicketItems() {
        var sum: Double
        var noOfItems: Int
        if (activity != null) {
            generalViewModel.getItems().observe(activity!!, Observer { tickets ->
                allTickets.clear()
                allTickets.addAll(tickets)
                if (tickets.isNotEmpty()) {
                    cartManenos.visibility = View.VISIBLE

                } else {
                    cartManenos.visibility = View.GONE
                }
                sum = 0.0
                for (i in tickets) {
                    sum += i.sell_price
                }
                totalTxt.text = sum.toString()
                noOfItems = 0
                for (i in allTickets) {
                    noOfItems += i.quantity
                }
                countTxt.text = noOfItems.toString()

            })

        }

    }


}