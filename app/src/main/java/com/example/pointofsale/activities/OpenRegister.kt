package com.example.pointofsale.activities

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.pointofsale.R
import com.example.pointofsale.models.OpenShift
import com.example.pointofsale.models.Shift
import com.example.pointofsale.models.Station
import com.example.pointofsale.models.User
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.getStationId
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.showToast
import com.example.pointofsale.utils.storeOpenningbalance
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_open_register.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class OpenRegister : AppCompatActivity() {
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var openShift: OpenShift
    private var user: User = User()
    private lateinit var loading: Dialog
    private var stid: String = ""
    val stationNames: MutableList<String> = arrayListOf()
    private var allShifts: MutableList<Shift> = arrayListOf()
    val allStations: MutableList<Station> = arrayListOf()
    var stationName: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_open_register)
        loading = Dialog(this)
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loading.setCancelable(false)
        loading.setContentView(R.layout.loading_dialog)
        loading.show()
        generalViewModel = ViewModelProvider(this).get(GeneralViewModel::class.java)
        getData()
        PushDownAnim.setPushDownAnimTo(opening_balance_btn)
            .setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
            stationName = stations_spinner.selectedItem.toString()
            if (stationName != "null" && stationName.isNotEmpty()) {
                for (i in allStations) {
                    if (i.name == stationName) {
                        stid = i.id.toString()
                    }
                }
            }
            val opening = opening_balance.text.toString().trim()
            if (opening.isEmpty() && cash.isVisible) {
                opening_balance.error = "Required"
                return@setOnClickListener
            } else if (stid.isEmpty() || stid == "null") {
                showToast("Please Select a station to proceed : $stid")
            } else {
                if (cash.isVisible) {
                    storeOpenningbalance(opb = opening, stid = stid)
                    openShift = OpenShift(opening.toDouble())
                    post()
                } else {
                    storeOpenningbalance(opb = "10", stid = stid)
//                    showToast(stid)
                    startActivity(Intent(this@OpenRegister, MainActivity::class.java))
                    finish()
                }

            }
        }
    }

    private fun post() {
        GlobalScope.launch(Dispatchers.Main) { openRegister() }
    }

    private suspend fun openRegister() {
        try {
            val response = ApiClient.webService.openShift(getToken(), openShift)
            if (response.isSuccessful) {
                showToast("Success")
                startActivity(Intent(this@OpenRegister, MainActivity::class.java))
                finish()
            }
        } catch (e: IOException) {

        }
    }

    private fun getData() {
        generalViewModel.getUserData().observe(this, Observer { u ->
            if (u != null) user = u
        })
        GlobalScope.launch(Dispatchers.Main) { getShifts() }
        generalViewModel.getStations().observe(this@OpenRegister, Observer { stations ->
            allStations.clear()
            allStations.addAll(stations)
            stationNames.clear()
            if (stations != null) {
                for (i in stations) {
                    stationNames.add(i.name)
                }
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                stationNames
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            stations_spinner.adapter = adapter
            stations_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    stationName = p0!!.getItemAtPosition(p2).toString()
                }

            }


        })


    }

    private suspend fun getShifts() {
        try {
            val response = ApiClient.webService.getShifts(this.getToken())
            if (response.isSuccessful) {
                allShifts.addAll(response.body()!!.shifts)
            }
        } catch (e: IOException) {
            loading.dismiss()
        }
        for (i in allShifts) {
            if (i.user.id == user.id && i.status == "open" && getStationId() != "null") {
                startActivity(Intent(this@OpenRegister, MainActivity::class.java))
//                showToast(getStationId())
                loading.dismiss()
                finish()
                break
            }
            if (i.user.id == user.id && i.status == "open" && getStationId() == "null") {
                cash.visibility = View.GONE
                loading.dismiss()
                break
            }
        }
        loading.dismiss()
    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    override fun onRestart() {
        super.onRestart()
        getData()
    }


}
