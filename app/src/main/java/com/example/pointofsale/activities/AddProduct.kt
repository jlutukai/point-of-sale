package com.example.pointofsale.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pointofsale.R
import com.example.pointofsale.adapters.ProductVariationAdapter
import com.example.pointofsale.models.*
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.isNetworkConnected
import com.example.pointofsale.utils.showToast
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_add_product.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import moe.feng.common.stepperview.VerticalStepperItemView
import java.io.File
import java.io.IOException

class AddProduct : AppCompatActivity() {
    private val mSteppers = arrayOfNulls<VerticalStepperItemView>(3)
    private lateinit var generalViewModel: GeneralViewModel
    private val subCategoryNames: MutableList<String> = arrayListOf()
    private val brands: MutableList<ManageBrand> = arrayListOf()
    private val units: MutableList<ManageUnit> = arrayListOf()
    private val categories: MutableList<Category> = arrayListOf()
    private val revenueType: MutableList<String> = arrayListOf("None", "Single", "Variable")
    private val applicableTax: MutableList<String> = arrayListOf("None", "VAT", "Custom")
    private val taxType: MutableList<String> = arrayListOf("None", "Inclusive", "Exclusive")
    private var variations: MutableList<ManageVariation> = arrayListOf()
    private var variation: ManageVariation? = null
    private val values: MutableList<Value> = arrayListOf()
    private lateinit var imageuri: Uri
    var imageresult: ArrayList<String> = arrayListOf()
    private var productName: String = ""
    private var productDesc: String = ""
    private var categoryId: Int = 0
    private var subCategoryId: Int = 0
    private var brandId: Int = 0
    private var unitId: Int = 0
    private var taxTypeName: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)

        setSupportActionBar(toolbar)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        generalViewModel = ViewModelProvider(this).get(GeneralViewModel::class.java)
        getCategories()
        mSteppers[0] = findViewById(R.id.stepper_0)
        mSteppers[1] = findViewById(R.id.stepper_1)
        mSteppers[2] = findViewById(R.id.stepper_2)
        VerticalStepperItemView.bindSteppers(*mSteppers)

        PushDownAnim.setPushDownAnimTo(img_product).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                Pix.start(this, Options.init().setRequestCode(100))
            }
        PushDownAnim.setPushDownAnimTo(next).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                firstSteps()
            }
        PushDownAnim.setPushDownAnimTo(back).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                mSteppers[1]!!.prevStep()
            }
        PushDownAnim.setPushDownAnimTo(next1).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                secondStep()
            }

        sub_category.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {
                }

                override fun onItemSelected(
                    p0: AdapterView<*>?,
                    p1: View?,
                    p2: Int,
                    p3: Long
                ) {
                    val selectedItem = p0!!.getItemAtPosition(p2).toString()
                    if (selectedItem != "None") {
                        for (i in categories) {
                            if (i.name == selectedItem) {
                                subCategoryId = i.id
                            }
                        }
                    }
                }
            }
    }

    private fun secondStep() {
        val cate = category.selectedItem.toString()
        var subCate = ""
//        val brandName = brand.selectedItem.toString()
//        val unitName = unit_spinner.selectedItem.toString()
        if (cate == "None") {
            showToast("Please select category")
            return
        }
        if (sub_category.selectedItem != null) {
            subCate = sub_category.selectedItem.toString()
        }
        if (subCategoryNames.size > 1 && subCate == "None") {
            showToast("Please select sub category")
            return
        }
//        if (brandName == "None"){
//            showToast("Please select brand")
//            return
//        }
//        if (unitName == "None"){
//            showToast("Please select unit")
//            return
//        }

//        for (i in brands){
//            if (i.name == brandName){
//                brandId = i.id
//            }
//        }
//        for (i in units){
//            if(unitName == "${i.actual_name} (${i.short_name})"){
//                unitId = i.id
//            }
//        }
        mSteppers[1]!!.nextStep()
    }

    private fun firstSteps() {
        productName = product_name.text.toString()
        productDesc = product_description.text.toString()
        if (productName.isEmpty()) {
            product_name.error = "Required"
            return
        }
        mSteppers[0]!!.nextStep()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 100) {
            imageresult = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
            imageuri = Uri.fromFile(File(imageresult[0]))
            img_product.setImageURI(imageuri)

        }
    }

    private fun getCategories() {
        val categoryNames: MutableList<String> = arrayListOf()

        GlobalScope.launch(Dispatchers.Main) { getData() }
        if (isNetworkConnected(this)) {
            generalViewModel.getUpdatedCategories()
        }
        generalViewModel.getLocalCategories().observe(this, Observer { cate ->
            if (cate.isNotEmpty()) {
                categories.addAll(cate)
                categoryNames.clear()
                categoryNames.add("None")
                for (i in cate) {
                    if (i.parent_id == 0) categoryNames.add(i.name)
                }
                if (categories.isNotEmpty()) {
                    val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        categoryNames as List<CharSequence>
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    category.adapter = adapter
                    category.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(p0: AdapterView<*>?) {
                            }

                            override fun onItemSelected(
                                p0: AdapterView<*>?,
                                p1: View?,
                                p2: Int,
                                p3: Long
                            ) {
                                val selectedItem = p0!!.getItemAtPosition(p2).toString()
                                if (selectedItem != "None") {

                                    for (i in categories) {
                                        if (i.name == selectedItem) {
                                            categoryId = i.id
                                        }
                                    }
                                    subCategoryNames.clear()
                                    subCategoryNames.add("None")
                                    for (i in categories) {
                                        if (i.parent_id == categoryId) {
                                            subCategoryNames.add(i.name)
                                            sub.visibility = View.VISIBLE
                                        }
                                    }
                                } else {
                                    subCategoryNames.clear()
                                    subCategoryNames.add("None")
                                }
                            }
                        }
                    val adapter1: ArrayAdapter<CharSequence> = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        subCategoryNames as List<CharSequence>
                    )
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    sub_category.adapter = adapter1
                } else {
                    showToast("No Categories found. Please add categories to continue")
                }
            }
        })

    }

    private suspend fun getData() {
        val unitNames: MutableList<String> = arrayListOf()
        unitNames.add("None")
        val brandNames: MutableList<String> = arrayListOf()
        brandNames.add("None")
        val varietyNames: MutableList<String> = arrayListOf()
        varietyNames.add("None")
        try {
            val response = ApiClient.webService.getManageBrands(getToken())
            if (response.isSuccessful) {
                val res = response.body()
                res?.let {
                    brands.clear()
                    brands.addAll(it.brands)
                    brandNames.clear()
                    brandNames.add("None")
                    for (i in brands) {
                        brandNames.add(i.name)
                    }
                    val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        brandNames as List<CharSequence>
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    brand.adapter = adapter
                }
            } else {
                showToast("Failed : ${response.code()}")
            }
        } catch (e: IOException) {
            showToast("Error : ${e.message}")
        }

        try {
            val response = ApiClient.webService.getManageUnits(getToken())
            if (response.isSuccessful) {
                val res = response.body()
                res?.let {
                    units.clear()
                    units.addAll(it.units)
                    unitNames.clear()
                    unitNames.add("None")
                    for (i in units) {
                        unitNames.add("${i.actual_name} (${i.short_name})")
                    }
                    val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
                        this,
                        android.R.layout.simple_spinner_item,
                        unitNames as List<CharSequence>
                    )
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                    unit_spinner.adapter = adapter
                }
            } else {
                showToast("Failed : ${response.code()}")
            }
        } catch (e: IOException) {
            showToast("Error : ${e.message}")
        }

        try {
            val response = ApiClient.webService.getManageVaiations(getToken())
            if (response.isSuccessful) {
                val res = response.body()
                res?.let {
                    variations.clear()
                    variations.addAll(it.variations)
                    varietyNames.clear()
                    varietyNames.add("None")
                    for (i in variations) {
                        varietyNames.add(i.name)
                    }
                }
            } else
                showToast("Error : ${response.code()}")
        } catch (e: IOException) {
            showToast("Error : ${e.message}")
        }

        val adapter4: ArrayAdapter<CharSequence> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            varietyNames as List<CharSequence>
        )
        adapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        variation_temps.adapter = adapter4
        variation_temps.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            @SuppressLint("DefaultLocale")
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val varietyName = parent!!.getItemAtPosition(position).toString()
                values.clear()
                for (i in variations) {
                    if (i.name.toLowerCase() == varietyName.toLowerCase()) {
                        variation = i
                        Log.d("add_product_var", "${i.name} $i")
                        values.addAll(i.values)
                    } else {
                        variation = null
                    }
                }

                if (values.isNotEmpty()) {
                    val valueVariations: MutableList<ProductVariationAdd> = arrayListOf()
                    for (i in values) {
                        val valueVariation = ProductVariationAdd(
                            id = i.id,
                            name = i.name,
                            sku = "",
                            purchase_price_exc_tax = 0.00,
                            purchase_price_inc_tax = 0.00,
                            margin = 25.0,
                            sell_price = 0.0
                        )
                        valueVariations.add(valueVariation)
                    }

                    val adapter = ProductVariationAdapter(this@AddProduct, valueVariations)
                    rv_variations.layoutManager = LinearLayoutManager(this@AddProduct)
                    rv_variations.isNestedScrollingEnabled = true
                    rv_variations.adapter = adapter
                    adapter.onItemClickEdit = {

                    }
                    adapter.onItemClickDelete = {


                    }
                }
            }

        }

        val adapter5: ArrayAdapter<CharSequence> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            applicableTax as List<CharSequence>
        )
        adapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        applicable_tax.adapter = adapter5

        unitNames.clear()
        unitNames.add("None")
        val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            unitNames as List<CharSequence>
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        unit_spinner.adapter = adapter
        val adapter2: ArrayAdapter<CharSequence> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            applicableTax as List<CharSequence>
        )
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        applicable_tax.adapter = adapter2

        val adapter3: ArrayAdapter<CharSequence> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            taxType as List<CharSequence>
        )
        adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        tax_type.adapter = adapter3
        tax_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                taxTypeName = parent!!.getItemAtPosition(position).toString()
                if (taxTypeName == "Inclusive") {
                    sellingprice.hint = "Selling Price (Inc. Tax)"
                }
                if (taxTypeName == "Exclusive") {
                    sellingprice.hint = "Selling Price (Exc. Tax)"
                }

            }

        }

        val adapter1: ArrayAdapter<CharSequence> = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            revenueType as List<CharSequence>
        )
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        product_type.adapter = adapter1
        product_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val selectedItem = parent!!.getItemAtPosition(position).toString()
                if (selectedItem == "None") {
                    variations_template.visibility = View.GONE
                    rv_variations.visibility = View.GONE
                    details_single.visibility = View.GONE
                }
                if (selectedItem == "Single") {
                    details_single.visibility = View.VISIBLE
                    variations_template.visibility = View.GONE
                    rv_variations.visibility = View.GONE
                }
                if (selectedItem == "Variable") {
                    variations_template.visibility = View.VISIBLE
                    rv_variations.visibility = View.VISIBLE
                    details_single.visibility = View.GONE
                }
            }

        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
