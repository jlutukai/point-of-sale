package com.example.pointofsale.activities

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Window
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.multidex.MultiDex
import com.example.pointofsale.R
import com.example.pointofsale.models.Message
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.RoomDbInit
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.*
import com.github.nikartm.button.FitButton
import com.google.gson.Gson
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import sunmi.paylib.SunmiPayKernel
import java.io.IOException

class Login : BaseActivity() {
    private lateinit var loading: Dialog
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var mSMPayKernel: SunmiPayKernel
    private lateinit var login: FitButton
    var isDisConnectService: Boolean = true
    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
        setContentView(R.layout.activity_login)
        connectPayService()
        loading = Dialog(this)
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE)
        loading.setCancelable(false)
        loading.setContentView(R.layout.loading_dialog)
        login = findViewById(R.id.login_btn)
        generalViewModel = ViewModelProvider(this).get(GeneralViewModel::class.java)
        PushDownAnim.setPushDownAnimTo(login).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
//                if (isDisConnectService) {
//                    connectPayService()
//                    showToast("Connecting to Pay Service")
//                    return@setOnClickListener
//                }
//                if (!isDisConnectService) {
                    loading.show()
                    doInBg()

//                }
            }
    }

    private fun doInBg() {
        GlobalScope.launch(Dispatchers.Main) {
            loginUser()
        }
    }

    private fun connectPayService() {
        mSMPayKernel = SunmiPayKernel.getInstance()
        mSMPayKernel.initPaySDK(this, mConnectCallback)
//        Log.d("connection", "$mSMPayKernel  $mConnectCallback")
    }

    private val mConnectCallback: SunmiPayKernel.ConnectCallback =
        object : SunmiPayKernel.ConnectCallback {
            override fun onConnectPaySDK() {
                try {
                    RoomDbInit.mReadCardOptV2 = mSMPayKernel.mReadCardOptV2
                    Log.d("connection", "on connect ${RoomDbInit.mReadCardOptV2}")
                    isDisConnectService = false
                } catch (e: Exception) {
                    e.printStackTrace()
                    Log.d("connection error : ", "${e.message}")
                }
            }

            override fun onDisconnectPaySDK() {
                isDisConnectService = true
                Log.d("connection", "on disconnect")
                showToast("Connection Failed")
            }

        }

    private fun loginUser() {
        val name = username.text.toString().trim()
        val pass = password.text.toString().trim()

        if (name.isEmpty()) {
            username.error = "REQUIRED"
            loading.dismiss()
            return
        }
        if (pass.isEmpty()) {
            password.error = "REQUIRED"
            loading.dismiss()
            return
        }
        if (isNetworkConnected(this)) {
            generalViewModel.loginUser(username = name, password = pass)
            login(name, pass)
        }
        generalViewModel.getUserData().observe(this, Observer { user ->
            if (user != null) {
                generalViewModel.getUpdatedBussinessDetails(user.business_id.toString())
            }
        })
        if (!isNetworkConnected(this) && getUname() == name && getPass() == pass) {
            openNext()
        } else if (!isNetworkConnected(this) && getUname() != name || getPass() != pass) {
            loading.dismiss()
            showToast("Wrong Credentials Please Connect to Internet and Try Again...")
        }


    }

    private fun login(username: String, password: String) {
        GlobalScope.launch(Dispatchers.Main) {
            try {
                val response = ApiClient.webService.loginUser(username, password)
                if (response.isSuccessful) {
                    val token = response.body()
                    token?.let {
                        storeToken("Bearer ${token.token}")
                        storeUsername(username = username, pass = password)
                        openNext()
                    }

                } else {
                    val gson = Gson()
                    val errorResponse = gson.fromJson<Message>(
                        response.errorBody()!!.charStream(),
                        Message::class.java
                    )
                    val msg = errorResponse.error
                    showToast(msg)
                    loading.dismiss()
                }
            } catch (e: IOException) {
                //error
                showToast("Error : ${e.message}")
                loading.dismiss()
            }
        }
    }

    private fun openNext() {

        if (getOpeningBalance() != "null" && getOpeningBalance().isNotEmpty()) {
            startActivity(Intent(this@Login, MainActivity::class.java))
            loading.dismiss()
            finish()
        } else {
            startActivity(Intent(this, OpenRegister::class.java))
            loading.dismiss()
            finish()
        }
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
}
