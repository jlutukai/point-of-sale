package com.example.pointofsale.activities

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pointofsale.R
import com.example.pointofsale.models.CloseShift
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.utils.clearOpeningBalance
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.showToast
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.android.synthetic.main.activity_end_shift.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.io.IOException

class EndShift : AppCompatActivity() {
    private lateinit var imageuri: Uri
    private lateinit var closeShift: CloseShift
    var amount = ""
    var cardslips = ""
    var chequesTra = ""
    var note = ""
    var imageresult: ArrayList<String> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_end_shift)
        setSupportActionBar(toolbar_e)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        img.setOnClickListener {
            Pix.start(this, Options.init().setRequestCode(100))
        }

        PushDownAnim.setPushDownAnimTo(submit).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {

                amount = amount_closing.text.toString().trim()
                cardslips = card_closing.text.toString().trim()
                chequesTra = cheques_closing.text.toString().trim()
                note = note_closing.text.toString().trim()

                if (amount.isEmpty()){
                    amount_closing.error = "Required"
                    return@setOnClickListener
                }
                closeShift = CloseShift(amount, cardslips, chequesTra, note)
                post()
            }

    }

    private fun post() {
        GlobalScope.launch(Dispatchers.Main) { closeshift() }
    }

    private suspend fun closeshift() {
        val id = intent.getStringExtra("id")

        try {
            val response = ApiClient.webService.closeShift(getToken(),id!!,closeShift)
            if (response.isSuccessful){
                showToast("Success")
                clearOpeningBalance()
                startActivity(Intent(this, OpenRegister::class.java))
                finish()
            }
        }catch (e:IOException){
            //error
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 100) {
            imageresult = data!!.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

            imageuri = Uri.fromFile(File(imageresult[0]))
            img.setImageURI(imageuri)

        }
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}
