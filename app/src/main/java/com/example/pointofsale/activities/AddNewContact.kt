package com.example.pointofsale.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pointofsale.R
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_add_new_contact.*

class AddNewContact : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_contact)
        setSupportActionBar(toolbar_c)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        PushDownAnim.setPushDownAnimTo(add_contact).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                val nameC: String = name.text.toString().trim()
                val email: String = mail.text.toString().trim()
                val phoneC: String = phone.text.toString().trim()
                when {
                    nameC.isEmpty() -> name.error = "Required"
                    email.isEmpty() -> mail.error = "Required"
                    phoneC.isEmpty() -> phone.error = "Required"
                    else -> addContact()
                }
            }
    }

    private fun addContact() {

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
