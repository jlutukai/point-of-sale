package com.example.pointofsale.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.pointofsale.R
import com.example.pointofsale.models.Message
import com.example.pointofsale.models.PaymentLine
import com.example.pointofsale.models.Transaction
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.utils.getToken
import com.example.pointofsale.utils.showToast
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_transaction_details.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.IOException

class TransactionDetails : AppCompatActivity() {
    private var allTransactions: MutableList<Transaction> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transaction_details)
        setSupportActionBar(toolbar_t)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)
        getData()

    }

    private fun getData() {
        GlobalScope.launch(Dispatchers.Main) { getTransactions() }
    }

    private suspend fun getTransactions() {
        try {
            val response = ApiClient.webService.getTransactions(getToken())
            if (response.isSuccessful){
                val transactions = response.body()
                transactions?.let {
                    allTransactions.addAll(it.transactions)
                    setData()
                }
            }else{
                val gson = Gson()
                val errorResponse = gson.fromJson<Message>(
                    response.errorBody()!!.charStream(),
                    Message::class.java
                )
                val msg = errorResponse.error
                showToast(msg)
            }
        }catch (e: IOException){
            Log.d("transactions", "error : $e")
        }
    }

    private fun setData() {
        val id = intent!!.getStringExtra("id")
        var totalPaid : Double= 0.00
        showToast(id!!)
        for (i in allTransactions){
            if (i.id == id!!.toInt()){
                val lines : MutableList<PaymentLine> = arrayListOf()
                lines.addAll(i.payment_lines)
                for (p in lines){
                    totalPaid += p.amount.toDouble()
                }
                invoice_no.text = i.invoice_no
                customer.text = i.contact?.name
                status.text = i.status
                total.text = i.final_total
                amount_paid.text = totalPaid.toString()
                payment_due.text = totalPaid.minus(i.final_total.toDouble()).toString()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
