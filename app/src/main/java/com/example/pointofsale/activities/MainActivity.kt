package com.example.pointofsale.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ExpandableListView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.ui.AppBarConfiguration
import com.example.pointofsale.R
import com.example.pointofsale.adapters.ExpandableAdapter
import com.example.pointofsale.fragments.*
import com.example.pointofsale.models.Nav
import com.example.pointofsale.room.RoomDbInit
import com.example.pointofsale.utils.clearLoginDetails
import com.example.pointofsale.utils.showToast
import com.google.android.material.navigation.NavigationView
import sunmi.paylib.SunmiPayKernel


class MainActivity : AppCompatActivity(), ExpandableListView.OnGroupClickListener,
    ExpandableListView.OnChildClickListener, NavigationView.OnNavigationItemSelectedListener {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var myExpandableListView: ExpandableListView
    private lateinit var myExpandableListAdapter: ExpandableAdapter
    private lateinit var toolbar: Toolbar
    private lateinit var drawerLayout  :DrawerLayout
    private  var parentList: MutableList<Nav> = arrayListOf()
    private var childList: HashMap<Nav, List<Nav>> = HashMap()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
         toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val navView: NavigationView = findViewById(R.id.nav_view)
        setData()
        myExpandableListView = findViewById(R.id.expanded_menu)
        myExpandableListView.setGroupIndicator(null)
        myExpandableListView.setAdapter(myExpandableListAdapter)
        myExpandableListView.setOnGroupClickListener(this)
        myExpandableListView.setOnChildClickListener(this)

        drawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this,
            drawerLayout,
            toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
//        appBarConfiguration = AppBarConfiguration(
//            setOf(
//                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow,
//                R.id.nav_tools,R.id.nav_help, R.id.nav_logout
//            ), drawerLayout
//        )
//        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setNavigationItemSelectedListener(this)
        if (savedInstanceState == null) {
                replaceFragment(HomeFragment())
                toolbar.title = "Sale"
        }
    }

    private fun setData() {
//        parentList.add(Nav("Dashboard", R.drawable.ic_sales))
        parentList.add(Nav("Sale", R.drawable.ic_sales))
        parentList.add(Nav("Items", R.drawable.ic_add_to_photos_black_24dp))
        parentList.add(Nav("Transactions", R.drawable.ic_receipt))
        parentList.add(Nav("Shifts", R.drawable.ic_list))
        parentList.add(Nav("Settings", R.drawable.ic_settings))

        val child = Nav("Categories", R.drawable.ic_sales)
        val child1 = Nav("Products", R.drawable.ic_sales)
        val child2 = Nav("Variations", R.drawable.ic_sales)
        val child3 = Nav("Brands", R.drawable.ic_sales)
        val child4 = Nav("Units", R.drawable.ic_sales)
        val childList1 : List<Nav> = listOf(child,child1,child2, child3, child4)
        childList[parentList[1]] = childList1

        myExpandableListAdapter = ExpandableAdapter(this, parentList, childList)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_sync -> {
                sync()
                true
            }
            R.id.action_close_register -> {
                closeRegister()
                true
            }
            R.id.action_logout -> {
                logout()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }

    }

    private fun logout() {
        clearLoginDetails()
        startActivity(Intent(this@MainActivity, Login::class.java))
        finish()
    }

    private fun closeRegister() {
        startActivity(Intent(this@MainActivity, EndShift::class.java))
        finish()
    }

    private fun sync() {
        showToast("Synchronizing...")
    }

    override fun onGroupClick(
        parent: ExpandableListView?,
        v: View?,
        groupPosition: Int,
        id: Long
    ): Boolean {
        when(groupPosition){
            0 -> {
                replaceFragment(HomeFragment())
                toolbar.title = "Sale"
                drawerLayout.closeDrawer(GravityCompat.START)
            }
            1 ->{

            }
            2 ->{
                replaceFragment(TransactionsFragment())
                toolbar.title = "Transactions"
                drawerLayout.closeDrawer(GravityCompat.START)
            }
            3 -> {
                replaceFragment(ShiftFragment())
                toolbar.title = "Shifts"
                drawerLayout.closeDrawer(GravityCompat.START)
            }
            4 -> {
                replaceFragment(ShareFragment())
                toolbar.title = "Settings"
                drawerLayout.closeDrawer(GravityCompat.START)
            }
        }

        return false
    }



    override fun onChildClick(
        parent: ExpandableListView?,
        v: View?,
        groupPosition: Int,
        childPosition: Int,
        id: Long
    ): Boolean {
        if (childList[parentList[groupPosition]] != null){
            when(childPosition){
                0 ->{
                    replaceFragment(CategoriesFragment())
                    toolbar.title = childList[parentList[groupPosition]]!![childPosition].name
                }
                1 ->{
                    replaceFragment(ProductsFragment())
                    toolbar.title = childList[parentList[groupPosition]]!![childPosition].name
                }
                2 ->{
                    replaceFragment(VariationsFragment())
                    toolbar.title = childList[parentList[groupPosition]]!![childPosition].name
                }
                3 ->{
                    replaceFragment(BrandsFragment())
                    toolbar.title = childList[parentList[groupPosition]]!![childPosition].name
                }
                4 ->{
                    replaceFragment(UnitFragment())
                    toolbar.title = childList[parentList[groupPosition]]!![childPosition].name
                }
            }
            drawerLayout.closeDrawer(GravityCompat.START)
        }
        return false
    }

    fun reStart(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    private fun replaceFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame, fragment)
        transaction.commit()
    }

    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        val drawer =
            findViewById<View>(R.id.drawer_layout) as DrawerLayout
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

}
