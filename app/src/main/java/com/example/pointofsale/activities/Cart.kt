package com.example.pointofsale.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.nfc.NfcAdapter
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.deishelon.roundedbottomsheet.RoundedBottomSheetDialog
import com.example.pointofsale.R
import com.example.pointofsale.adapters.TicketAdapter
import com.example.pointofsale.models.*
import com.example.pointofsale.reciever.SmsListener
import com.example.pointofsale.retrofit.ApiClient
import com.example.pointofsale.room.RoomDbInit
import com.example.pointofsale.room.viewModel.GeneralViewModel
import com.example.pointofsale.utils.*
import com.github.nikartm.button.FitButton
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.nbbse.printapi.Printer
import com.sunmi.pay.hardware.aidl.AidlConstants
import com.sunmi.pay.hardware.aidlv2.readcard.CheckCardCallbackV2
import com.thekhaeng.pushdownanim.PushDownAnim
import kotlinx.android.synthetic.main.activity_cart.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.fabiomsr.moneytextview.MoneyTextView
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*


class Cart : AppCompatActivity() {

    private var aidlUtils = AidlUtils().getInstance()
    private var nfcAdapter: NfcAdapter? = null
    private lateinit var pendingIntent: PendingIntent
    private lateinit var generalViewModel: GeneralViewModel
    private lateinit var paymentM: String
    private lateinit var dialog1: Dialog
    var businessName: String = ""
    var uid: String = ""
    var sum: Double = 0.0
    var paidCash: Double = 0.0
    private lateinit var cust_id: String
    private lateinit var bitmap: Bitmap
    private lateinit var bitmap1: Bitmap
    private lateinit var sn: EditText
    private var trans : EditText? = null
    private lateinit var printer: Printer
    private var allTickets: MutableList<Ticket> = arrayListOf()
    private var lat: String = ""
    private var pc: String = ""
    private var serial: String = ""
    private var long: String = ""
    private var url: String = ""

    private var r: String? = null
    private var transCode = ""
    private var datalist: MutableList<TableItem> = arrayListOf()
    private var datalist1: MutableList<TableItem> = arrayListOf()
    private var datalist2: MutableList<TableItem> = arrayListOf()
    private var datalist3: MutableList<TableItem> = arrayListOf()
    private var datalist4: MutableList<TableItem> = arrayListOf()
    private val permissionRequestAccessFineLocation = 100
    private lateinit var mHandler: Handler


    override fun onCreate(savedInstanceState: Bundle?) {
//        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
//        window.setFlags(
//            WindowManager.LayoutParams.FLAG_FULLSCREEN,
//            WindowManager.LayoutParams.FLAG_FULLSCREEN
//        )
        setContentView(R.layout.activity_cart)
        setSupportActionBar(toolbar_b)
        val appbar = supportActionBar
        appbar!!.setDisplayHomeAsUpEnabled(true)
        appbar.setDisplayShowHomeEnabled(true)

        aidlUtils.initPrinter(this)
        generalViewModel = ViewModelProvider(this).get(GeneralViewModel::class.java)
        cust_id = intent.getStringExtra("cid")!!
        doInBg(this)
        getTickets(this)
        PushDownAnim.setPushDownAnimTo(charge).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
            .setOnClickListener {
                enableBroadcastReceiver()
                if (allTickets.isNotEmpty()) {
                    createBill("closed")
                } else {
                    showToast("No Products In Cart")
                }
            }

//        nfcAdapter = NfcAdapter.getDefaultAdapter(this)
//        if (nfcAdapter == null) {
//            showToast("No NFC")
//            finish()
//            return
//        }
//        pendingIntent = PendingIntent.getActivity(
//            this,
//            0,
//            Intent(this, this.javaClass).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),
//            0
//        )

//        PushDownAnim.setPushDownAnimTo(suspend).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
//            .setOnClickListener {
//                createBill("open")
//            }

    }


    private fun showWirelessSettings() {
        showToast("You need to enable NFC")
        val intent = Intent(Settings.ACTION_WIRELESS_SETTINGS)
        startActivity(intent)
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        if (intent != null) {
            resolveIntent(intent)
        }
    }

    private fun resolveIntent(intent: Intent) {
        val action = intent.action
        if (NfcAdapter.ACTION_TAG_DISCOVERED == action || NfcAdapter.ACTION_TECH_DISCOVERED == action || NfcAdapter.ACTION_NDEF_DISCOVERED == action) {
            val id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID)
            var hexdump = String()
            for (i in id.indices) {
                var x =
                    Integer.toHexString(id[i].toInt() and 0xff)
                if (x.length == 1) {
                    x = "0$x"
                }
                hexdump += "$x "
            }
            serial = hexdump.replace("\\s".toRegex(), "")
            Log.d("NFC", "$serial")

        }
    }

    private fun doInBg(context: Context) {
//        getLocationDetails()
    }


    override fun onResume() {
        super.onResume()
        enableBroadcastReceiver()
        mHandler = Handler()
        if (nfcAdapter != null) {
            if (!nfcAdapter!!.isEnabled)
                showWirelessSettings()
            nfcAdapter!!.enableForegroundDispatch(this, pendingIntent, null, null)
        }
    }

    private fun getLocationDetails() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this as Activity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                permissionRequestAccessFineLocation
            )
            return
        }

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
        val locationListener = object : LocationListener {

            override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {

            }

            override fun onProviderEnabled(p0: String?) {
            }

            override fun onProviderDisabled(p0: String?) {

            }

            override fun onLocationChanged(p0: Location?) {
                val latitude = p0!!.latitude
                lat = latitude.toString()
                val longitude = p0.longitude
                long = longitude.toString()
//                showToast("$long $lat")
            }
        }


        locationManager?.requestLocationUpdates(
            LocationManager.NETWORK_PROVIDER,
            0L,
            0f,
            locationListener
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == permissionRequestAccessFineLocation) {
            when (grantResults[0]) {
                PackageManager.PERMISSION_GRANTED -> getLocationDetails()
                PackageManager.PERMISSION_DENIED -> showToast("enable location")
            }
        }
    }

    private fun getTickets(context: Context) {


        generalViewModel.getLocalBusinessDetails()
            .observe(this, Observer { biz -> if (biz != null) businessName = biz.name })
        generalViewModel.getUserData()
            .observe(this, Observer { user -> if (user != null) uid = user.id.toString() })
        generalViewModel.getItems().observe(this, Observer { tickets ->
            allTickets.clear()
            allTickets.addAll(tickets)
            val recycler = findViewById<RecyclerView>(R.id.rv_ticket_items)
            recycler.layoutManager = LinearLayoutManager(this)
            val ticketAdapter = TicketAdapter(this, tickets as MutableList<Ticket>)
            recycler.adapter = ticketAdapter
            ticketAdapter.notifyDataSetChanged()
            sum = 0.0
            for (i in tickets) {
                sum += i.sell_price
            }
            val totalPrice = findViewById<MoneyTextView>(R.id.totals_cart)
            totalPrice.amount = sum.toFloat()
            ticketAdapter.onItemClickAdd = {
                if (it.product.enable_sr_no == 1) {
                    showNfcDialog(it, "add")
                    checkCard()
                }
                if (it.product.enable_sr_no == 0) {
                    val qty = it.quantity + 1
                    val price = it.default_sell_price.toDouble()
                    val newPrice = price * qty
                    generalViewModel.updateQty(it, qty)
                    generalViewModel.updateItemPrice(it, newPrice)
                }
            }
            ticketAdapter.onItemClickSub = {
                if (it.quantity <= 1) {
                    generalViewModel.deletItem(it)
                }
                if (it.product.enable_sr_no == 1 && it.quantity > 1) {
                    showNfcDialog(it, "sub")
                    checkCard()

                }
                if (it.product.enable_sr_no == 0 && it.quantity > 1) {
                    val qty = it.quantity
                    if (qty > 1) {
                        val newQty = it.quantity - 1
                        val price = it.default_sell_price.toDouble()
                        val newPrice = price * newQty
                        generalViewModel.updateQty(it, newQty)
                        generalViewModel.updateItemPrice(it, newPrice)
                    }
                }
            }
        })
    }


    private fun checkCard() {
        mHandler = Handler(Looper.getMainLooper())
        Log.d("connection", "checkcard")
        try {
            val cardType: Int = AidlConstants.CardType.MIFARE.value
            RoomDbInit.mReadCardOptV2!!.checkCard(cardType, mCheckCardCallback, 60)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val mCheckCardCallback: CheckCardCallbackV2 = object : CheckCardCallbackV2Wrapper() {

        override fun findRFCard(p0: String?) {
            handleResult(p0)
        }

        override fun onError(p0: Int, p1: String?) {
            if (p1 != null) showToast("Error $p0 : $p1")
            checkCard()
        }
    }

    private fun handleResult(uuid: String?) {
        if (isFinishing) {
            return
        }
        mHandler.post {
            Log.d("connection", "result $uuid")
            sn.setText(uuid)
            if (!isFinishing) {
                mHandler.postDelayed(::checkCard, 5000)
            }
        }
    }


    private fun showNfcDialog(ticket: Ticket, s: String) {
        dialog1 = Dialog(this)
        dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog1.setCancelable(true)
        dialog1.setContentView(R.layout.dialog_add_stuff)
        val title = dialog1.findViewById<TextView>(R.id.title_dialog)
        title.text = "Add Asset Tag"
        sn = dialog1.findViewById<EditText>(R.id.serials)
        sn.visibility = View.VISIBLE
        val close = dialog1.findViewById<FitButton>(R.id.close_dialog_add)
        close.setOnClickListener {
            dialog1.dismiss()
        }
        val submit = dialog1.findViewById<FitButton>(R.id.submit_dialog_add)
        submit.setOnClickListener {
            var qty: Int
            if (sn.text.isEmpty()) {
                sn.error = "Approach tag to get serial"
                return@setOnClickListener
            }
            if (s == "add") {
                qty = ticket.quantity + 1
                val price = ticket.default_sell_price.toDouble()
                val newPrice = price * qty
                generalViewModel.addAsset(ticket, sn.text.toString(), qty, newPrice, this)
                dialog1.dismiss()
            }
            if (s == "sub") {
                qty = ticket.quantity
                if (qty > 1) {
                    val newQty = ticket.quantity - 1
                    val price = ticket.default_sell_price.toDouble()
                    val newPrice = price * newQty
                    generalViewModel.removeAsset(ticket, sn.text.toString(), newQty, newPrice, this)
                    dialog1.dismiss()
                }
            }
        }
        dialog1.show()
        val window = dialog1.window
        window!!.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

   fun enableBroadcastReceiver()
    {
        val receiver : ComponentName =  ComponentName(this, SmsListener::class.java)
        val pm  :PackageManager = this.packageManager
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP)
       }

    fun disableBroadcastReceiver(){
        val receiver : ComponentName =  ComponentName(this, SmsListener::class.java)
        val pm  :PackageManager = this.packageManager
        pm.setComponentEnabledSetting(receiver,
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP)
    }

    private fun createBill(s: String) {
        val contid = intent.getStringExtra("cid")
        val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
        val currentDate = sdf.format(Date())

        paymentM = ""
        val paymentMethods: MutableList<String> = arrayListOf("Cash", "M-Pesa", "Card", "Cheque")
        if (s == "closed") {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            val dialog = RoundedBottomSheetDialog(this)
            val sheetView = layoutInflater.inflate(R.layout.payment_dialog, null)
//            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//            dialog.setCancelable(true)
            dialog.setContentView(sheetView)
            val cashDetails = dialog.findViewById<LinearLayout>(R.id.cash_details)
            trans = dialog.findViewById(R.id.trans_code)

            val spinner = dialog.findViewById<Spinner>(R.id.payment)
            val balance = dialog.findViewById<EditText>(R.id.balance)
            val paid = dialog.findViewById<EditText>(R.id.cash)
            paid?.setOnFocusChangeListener { view, _ ->
                if (paid?.text.isNotEmpty() && view.isFocused) {
                    paidCash = paid?.text.toString().toDouble()
                    balance?.setText(df.format(paidCash.minus(sum)).toString())
                }
            }
            balance?.setOnClickListener {
                if (paid?.text.toString().isNotEmpty()) {
                    paidCash = paid?.text.toString().toDouble()
                    balance.setText(df.format(paidCash.minus(sum)).toString())
                }
            }
            val adapter: ArrayAdapter<String> = ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                paymentMethods as List<String>
            )
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner?.adapter = adapter
            spinner?.onItemSelectedListener =
                object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {
                    }

                    override fun onItemSelected(
                        p0: AdapterView<*>?,
                        p1: View?,
                        p2: Int,
                        p3: Long
                    ) {
                        paymentM = p0!!.getItemAtPosition(p2).toString()
                        if (paymentM == "Cash") {
                            cashDetails?.visibility = View.VISIBLE
                        }else if (paymentM == "M-Pesa"){
                            paid?.visibility = View.GONE
                            trans?.visibility = View.VISIBLE
                            cashDetails?.visibility = View.GONE
                        } else {
                            paid?.setText("")
                            balance?.setText("")
                            cashDetails?.visibility = View.GONE
                        }
                    }
                }
            val totalDue = dialog.findViewById<EditText>(R.id.due)
            totalDue?.setText(sum.toString())


            val close = dialog.findViewById<RelativeLayout>(R.id.close_payment)
            PushDownAnim.setPushDownAnimTo(close).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                .setOnClickListener {
                    dialog.dismiss()
                }
            val proceed = dialog.findViewById<RelativeLayout>(R.id.proceed)
            PushDownAnim.setPushDownAnimTo(proceed).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
                .setOnClickListener {
                    pc = paid?.text.toString()
                    when {
                        pc.isEmpty() && paymentM == "Cash" -> {
                            paid?.error = "Required"
                            return@setOnClickListener
                        }
                        paymentM == "Cash" && sum > paid?.text.toString().toDouble()  -> {
                            paid?.error = "Not Enough"
                            return@setOnClickListener
                        }
                        paymentM == "M-Pesa" && transCode.isEmpty() -> {
                            showToast("Awaiting Mpesa Payment" )
                            return@setOnClickListener
                        }
                        else -> {
//                            showToast(transCode)
                            disableBroadcastReceiver()
                            postPayment(s)
                            dialog.dismiss()
                        }
                    }
                }
            dialog.show()
//            val window = dialog.window
//            window!!.setLayout(
//                ViewGroup.LayoutParams.MATCH_PARENT,
//                ViewGroup.LayoutParams.WRAP_CONTENT
//            )
        }


    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        enableBroadcastReceiver()
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
        disableBroadcastReceiver()
    }

    override fun onDestroy() {
        super.onDestroy()
        disableBroadcastReceiver()
    }

    override fun onPause() {
        super.onPause()
        disableBroadcastReceiver()
    }

    @Subscribe
    fun onSmsReceived(event: OnReceiverEvent) {
        val code = event.code
        transCode = code
        trans?.setText(code)
    }

    private fun postPayment(s: String) {
        val smpldf = SimpleDateFormat("dd/M/yyyy")
        val smptf = SimpleDateFormat("hh:mm:ss")
        val date = smpldf.format(Date())
        val time = smptf.format(Date())
        val payments: MutableList<Payment> = arrayListOf()
        val products: MutableList<ProdSale> = arrayListOf()
        for (i in allTickets) {
            val prodSale = ProdSale(
                unit_price = i.default_sell_price.toDouble(),
                line_discount_type = "fixed",
                line_discount_amount = 0.00,
                item_tax = 0.00,
                tax_id = "",
                sell_line_note = "",
                product_id = i.product_id,
                variation_id = i.id,
                enable_stock = 0,
                quantity = i.quantity.toDouble(),
                product_unit_id = i.product.unit_id,
                base_unit_multiplier = "1",
                assets = i.assets,
                unit_price_inc_tax = i.sell_price_inc_tax.toDouble()
            )
            products.add(prodSale)
        }
        if (s == "closed") {
            val payment = Payment(
                amount = sum,
                method = paymentM,
                account_id = "",
                card_number = "",
                card_holder_name = "",
                card_transaction_number = "",
                card_type = "",
                card_month = "",
                card_year = "",
                card_security = "",
                cheque_number = "",
                bank_account_number = "",
                transaction_no_1 = "$transCode",
                transaction_no_2 = "",
                transaction_no_3 = "",
                note = ""
            )
            payments.add(payment)
            val invoice = Invoices(
//                location_id = 1,
                location_id = getStationId().toInt(),
                price_group = 0,
                contact_id = cust_id.toInt(),
                pay_term_number = "",
                pay_term_type = "",
                search_product = "",
                res_table_id = "",
                sell_price_tax = "includes",
                products = products,
                narative = "",
                discount_type = "percentage",
                discount_amount = 0.00,
                tax_rate_id = "",
                tax_calculation_amount = 0.00,
                shipping_details = "",
                shipping_charges = 0.00,
                final_total = sum,
                discount_type_modal = "",
                discount_amount_modal = 0.00,
                order_tax_modal = "",
                shipping_details_modal = "",
                shipping_charges_modal = 0.00,
                payment = payments,
                sale_note = "",
                staff_note = "",
                change_return = "",
                additional_notes = "",
                is_suspend = 0,
                recur_interval = "",
                recur_interval_type = "days",
                recur_repetitions = "",
                status = "final"
            )
            GlobalScope.launch(Dispatchers.Main) { makePayment(invoice) }
        }
        if (s == "open") {
            val invoice = Invoices(
                location_id = getStationId().toInt(),
                price_group = 0,
                contact_id = cust_id.toInt(),
                pay_term_number = "",
                pay_term_type = "",
                search_product = "",
                res_table_id = "",
                sell_price_tax = "includes",
                products = products,
                narative = "",
                discount_type = "percentage",
                discount_amount = 0.00,
                tax_rate_id = "",
                tax_calculation_amount = 0.00,
                shipping_details = "",
                shipping_charges = 0.00,
                final_total = sum,
                discount_type_modal = "",
                discount_amount_modal = 0.00,
                order_tax_modal = "",
                shipping_details_modal = "",
                shipping_charges_modal = 0.00,
                payment = payments,
                sale_note = "",
                staff_note = "",
                change_return = "",
                additional_notes = "",
                is_suspend = 0,
                recur_interval = "",
                recur_interval_type = "days",
                recur_repetitions = "",
                status = "due"
            )
            GlobalScope.launch(Dispatchers.Main) {
                makePayment(invoice)
            }
        }

    }

    private suspend fun makePayment(invoice: Invoices) {
        try {
            val response = ApiClient.webService.makePayment(
                application.getToken(),
                "application/json",
                invoice
            )
            if (response.isSuccessful) {
                if (response.body()!!.success == 0) {
                    showToast(response.body()!!.msg)
                } else {
                    url = response.body()!!.url
                    r = response.body()!!.transaction.invoice_no
                    showToast(response.body()!!.msg)
                    printReceipt(url)
                }

            }
            if (response.code() == 429) {
                showToast("Server Error : Too many requests ")
            }
//            else {
//                val gson = Gson()
//                val errorResponse = gson.fromJson<Message>(
//                    response.errorBody()!!.charStream(),
//                    Message::class.java
//                )
//                val msg = errorResponse.error
//                application.showToast(msg)
//            }
        } catch (e: Exception) {
//            showToast("Error : $e")
            Log.d("Error ", ": ${e.message}")
        }
    }


    @SuppressLint("SimpleDateFormat")
    private fun printReceipt(c: String) {
        datalist.clear()
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        val totals: MutableList<Reciept> = arrayListOf()
        val cash: MutableList<Reciept> = arrayListOf()
        val rec: MutableList<Reciept> = arrayListOf()
        val smpldf = SimpleDateFormat("dd/M/yyyy")
        val smptf = SimpleDateFormat("HH:mm:ss")
        val date = smpldf.format(Date())
        val time = smptf.format(Date())
        val tax = df.format(sum * 0.16)
//        url = "hello"
        val value1 = Reciept("TOTAL", sum.toString())
        totals.add(0, value1)
        val value2 = Reciept("TOTAL A-16.00%", tax.toString())
        totals.add(1, value2)
        val value3 = Reciept("TOTAL B-EX", "0")
        totals.add(2, value3)
        val value4 = Reciept("TOTAL TAX A", "0")
        totals.add(3, value4)
        val value5 = Reciept("TOTAL TAX", tax.toString())
        totals.add(4, value5)
        val value6 = Reciept("$paymentM", sum.toString())
        cash.add(0, value6)
        if (paymentM == "M-Pesa") {
            val value8 = Reciept("Transaction Code", "$transCode")
            cash.add(1, value8)
            val value7 = Reciept("ITEMS NUMBER", allTickets.size.toString())
            cash.add(2, value7)
        }else{
            val value7 = Reciept("ITEMS NUMBER", allTickets.size.toString())
            cash.add(1, value7)
        }
        if (r != null) {
            val value8 = Reciept("RECEIPT NUMBER", r!!)
            rec.add(0, value8)
        }
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix: BitMatrix =
                multiFormatWriter.encode(c, BarcodeFormat.QR_CODE, 200, 200)
            val barcodeEncoder = BarcodeEncoder()
            bitmap = barcodeEncoder.createBitmap(bitMatrix)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
        datalist1.clear()
        for (i in totals) {
            printTotals(i)
        }
        datalist2.clear()
        for (i in cash) {
            printCash(i)
        }
        datalist3.clear()
        for (i in rec) {
            printRec(i)
        }
//        printer = Printer.getInstance()
        aidlUtils.setDarkness(1)
        aidlUtils.printTitleText(businessName, 40F, bold = true, underline = false)
        aidlUtils.printTitleText("P.O. Box 0001, Nairobi", 24F, bold = false, underline = false)
        aidlUtils.printTitleText("Welcome to our shop", 24F, bold = false, underline = false)
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText("PIN.: A0000000L", 24F, bold = false, underline = false)
        aidlUtils.printTitleText("Buyer PIN.: A0000001L", 24F, bold = false, underline = false)
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText("FISCAL RECEIPT", 26F, bold = true, underline = false)
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        for (i in allTickets) {
            printProducts(i)
        }
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTable(datalist1)
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTable(datalist2)
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText(
            "Control Unit Info",
            size = 24F,
            bold = true,
            underline = false
        )
        aidlUtils.printText(
            "Date: $date Time: $time"
        )
        aidlUtils.printTitleText(
            "CU Serial No. : KRAMW582201907007181",
            size = 24F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText(
            "CU Invoice No. : 58207181000003",
            size = 24F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printBitmap(bitmap)
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTable(datalist3)
        aidlUtils.printText(
            "Date: $date Time: $time"
        )
        aidlUtils.printTitleText(
            content = " - - - - - - - - - - - - - - - - - - - - -",
            size = 18F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText(
            "THANK YOU",
            size = 24F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText(
            "COME BACK AGAIN",
            size = 24F,
            bold = false,
            underline = false
        )
        aidlUtils.printTitleText(
            "YOUR BEST STORE IN TOWN",
            size = 24F,
            bold = false,
            underline = false
        )
        datalist.clear()
        aidlUtils.print3Line()
////        printer.printEndLine()
////
        generalViewModel.deleteTickets()
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    private fun printRec(i: Reciept) {
        val items: Array<String> = arrayOf(i.name, i.value)
        val width: IntArray = intArrayOf(20, 10)
        val align: IntArray = intArrayOf(0, 2)
        val tableItem = TableItem(items, width, align)
        datalist3.add(tableItem)
    }

    private fun printCash(i: Reciept) {
        val items: Array<String> = arrayOf(i.name, i.value)
        val width: IntArray = intArrayOf(20, 10)
        val align: IntArray = intArrayOf(0, 2)
        val tableItem = TableItem(items, width, align)
        datalist2.add(tableItem)
    }

    private fun printTotals(i: Reciept) {
        val items: Array<String> = arrayOf(i.name, i.value)
        val width: IntArray = intArrayOf(20, 10)
        val align: IntArray = intArrayOf(0, 2)
        val tableItem = TableItem(items, width, align)
        datalist1.add(tableItem)
    }

    private fun printProducts(ticket: Ticket) {
        datalist4.clear()
        datalist.clear()
        val s: String = ticket.name.trim()
        val items: Array<String> = arrayOf(s, "  ")
        val width: IntArray = intArrayOf(18, 10)
        val align: IntArray = intArrayOf(0, 2)
        val tableItem = TableItem(items, width, align)
        datalist4.add(tableItem)
        val items1: Array<String> = arrayOf(
            ticket.default_sell_price.trim(),
            ticket.quantity.toString(),
            ticket.sell_price.toString()
        )
        val width1: IntArray = intArrayOf(16, 4, 10)
        val align1: IntArray = intArrayOf(0, 1, 2)
        val tableItem1 = TableItem(items1, width1, align1)
        datalist.add(tableItem1)
        aidlUtils.printTable(datalist4)
        aidlUtils.printTable(datalist)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

}



