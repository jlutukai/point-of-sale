package com.example.pointofsale.activities

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.RemoteException
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.pointofsale.room.RoomDbInit
import com.example.pointofsale.utils.CheckCardCallbackV2Wrapper
import com.example.pointofsale.utils.showToast
import com.sunmi.pay.hardware.aidl.AidlConstants
import com.sunmi.pay.hardware.aidlv2.readcard.CheckCardCallbackV2
import kotlinx.android.synthetic.main.activity_product.*
import sunmi.paylib.SunmiPayKernel


class Product : AppCompatActivity() {
    private lateinit var mSMPayKernel: SunmiPayKernel
    private var isDisConnectService: Boolean = true
    private val mHandler: Handler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.example.pointofsale.R.layout.activity_product)
        connectPayService(this)

    }

    //
    fun connectPayService(context: Context) {
        Log.d("connection", "pay service")
        mSMPayKernel = SunmiPayKernel.getInstance()
        mSMPayKernel.initPaySDK(context, mConnectCallback)
    }

    private val mConnectCallback: SunmiPayKernel.ConnectCallback =
        object : SunmiPayKernel.ConnectCallback {
            override fun onConnectPaySDK() {
                try {
                    Log.d("connection", "on connect")
                    RoomDbInit.mReadCardOptV2 = mSMPayKernel.mReadCardOptV2
                    isDisConnectService = false
                    checkCard()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onDisconnectPaySDK() {
                isDisConnectService = true
                Log.d("connection", "on disconnect")
//                showToast(R.string.connect_fail)
            }

        }

    private fun checkCard() {
        Log.d("connection", "checkcard")
        try {
            val cardType: Int = AidlConstants.CardType.NFC.value
            RoomDbInit.mReadCardOptV2!!.checkCard(cardType, mCheckCardCallback, 600000)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private val mCheckCardCallback: CheckCardCallbackV2 = object : CheckCardCallbackV2Wrapper() {

        @Throws(RemoteException::class)
        override fun findRFCardEx(info: Bundle?) {
            Log.d("connection", "result")
            handleResult(1, info)
        }

        @Throws(RemoteException::class)
        override fun onErrorEx(info: Bundle?) {
            super.onErrorEx(info)
            Log.d("connection", "error ${info!!.getString("message")}")
            val code: Int = info.getInt("code")
            val msg: String? = info.getString("message")
            val error: String? = "onError:$msg -- $code"
            showToast(error!!)
            handleResult(-1, info)
        }
    }

    private fun handleResult(i: Int, info: Bundle?) {
        if (isFinishing) {
            return
        }
        mHandler.post {
            if (i == 1) { //find NFC
                Log.d("connection", "result ${info?.getString("uuid")}")
                uuid.text = info?.getString("uuid")
            }
            // 继续检卡
            if (!isFinishing) {
                mHandler.postDelayed(::checkCard, 5000)
            }
        }

    }


    override fun onDestroy() {
        mHandler.removeCallbacksAndMessages(null)
        cancelCheckCard()
        super.onDestroy()
    }

    private fun cancelCheckCard() {
        try {
            RoomDbInit.mReadCardOptV2!!.cardOff(AidlConstants.CardType.NFC.value)
            RoomDbInit.mReadCardOptV2!!.cancelCheckCard()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
