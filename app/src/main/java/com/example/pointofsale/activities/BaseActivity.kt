package com.example.pointofsale.activities

import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.example.pointofsale.room.RoomDbInit

abstract class BaseActivity : AppCompatActivity() {
    var baseApp : RoomDbInit = RoomDbInit()

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        baseApp = this.application as RoomDbInit
    }
}