package com.example.pointofsale.activities

import android.app.PendingIntent
import android.nfc.NfcAdapter
import android.os.Bundle
import android.os.Handler
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.pointofsale.R
import com.example.pointofsale.adapters.ProductsAdapter
import com.example.pointofsale.models.Contact
import com.example.pointofsale.models.Shift
import com.example.pointofsale.models.Ticket
import com.example.pointofsale.models.Variations
import com.example.pointofsale.room.viewModel.GeneralViewModel
import kotlinx.android.synthetic.main.activity_make_sale.*
import sunmi.paylib.SunmiPayKernel

class MakeSale : AppCompatActivity() {
    private lateinit var mSMPayKernel: SunmiPayKernel
    private var isDisConnectService: Boolean = true
    private val mHandler: Handler = Handler()
    private var nfcAdapter: NfcAdapter? = null
    private lateinit var pendingIntent: PendingIntent
    private lateinit var generalViewModel: GeneralViewModel
    private var productsAdapter: ProductsAdapter? = null
    private lateinit var rvProducts: RecyclerView
    private var cust_id: String? = null
    private var serial: String = ""
    //     private lateinit var  mScannerView  :ZXingScannerView
    private var allShifts: MutableList<Shift> = arrayListOf()
    private var openShift = 0
    private lateinit var walkinid: String
    private var allProducts: MutableList<Variations> = arrayListOf()
    private var allContacts: MutableList<Contact> = arrayListOf()
    private var allTickets: MutableList<Ticket> = arrayListOf()
    var searchList: MutableList<Variations> = arrayListOf()
    private lateinit var cartManenos: LinearLayout
    private lateinit var countTxt: TextView
    private lateinit var totalTxt: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_make_sale)
        setContentView(R.layout.activity_cart)
        setSupportActionBar(toolbar_sale)
//        val appbar = supportActionBar
//        appbar.setDisplayHomeAsUpEnabled(true)
//        appbar.setDisplayShowHomeEnabled(true)
//        generalViewModel = ViewModelProviders.of(this).get(GeneralViewModel::class.java)
//        getCategories()
//        rvProducts = findViewById(R.id.rv_products)
//        cartManenos = findViewById(R.id.cart_manenos)
//        countTxt = findViewById(R.id.count)
//        totalTxt = findViewById(R.id.totals)
//        rvProducts.layoutManager = GridLayoutManager(this, 3)
//        rvProducts.addItemDecoration(SpacingItemDecoration(3, dpToPx(this, 8), true))
//        rvProducts.setHasFixedSize(true)
//        rvProducts.isNestedScrollingEnabled = false
//        productsAdapter = ProductsAdapter(this, allProducts)
//        getProducts()
//        showTicketItems()
//        getContactsData()
//        PushDownAnim.setPushDownAnimTo(ticket).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
//            .setOnClickListener {
//                if (getStationId() == "null" || allShifts.size == 0) {
//                    startActivity(Intent(this, OpenRegister::class.java))
//                } else {
//                    showTicketItems()
//                    openDialog()
//                }
////                    v.expandable_layout.isExpanded = !v.expandable_layout.isExpanded
//            }
//        PushDownAnim.setPushDownAnimTo(add_customer).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
//            .setOnClickListener {
//
//                addCustomerToBill()
//
//            }
//        PushDownAnim.setPushDownAnimTo(barcode).setScale(PushDownAnim.MODE_STATIC_DP, 8F)
//            .setOnClickListener {
//                startActivity(Intent(this, BarcodeScanner::class.java))
//            }
//        val search = findViewById<androidx.appcompat.widget.SearchView>(R.id.search)
//        search.setOnQueryTextListener(object :
//            androidx.appcompat.widget.SearchView.OnQueryTextListener {
//            override fun onQueryTextSubmit(p0: String?): Boolean {
//                search.hideKeyboard()
//                return true
//            }
//
//            override fun onQueryTextChange(p0: String?): Boolean {
//                if (p0!!.isNotEmpty()) {
//                    searchList.clear()
//                    val char = p0.toLowerCase(Locale.getDefault())
//                    for (i in allProducts) {
//                        if (i.name.toLowerCase(Locale.getDefault()).contains(char)) {
//                            searchList.add(i)
//                        }
//                    }
//                    val adapter = ProductsAdapter(this@MakeSale, searchList)
//                    rvProducts.adapter = adapter
//                    adapter.onItemClick = {
//                        getProductVariations(it.id)
//                    }
//                } else {
//                    searchList.clear()
//                    searchList.addAll(allProducts)
//                    val adapter = ProductsAdapter(this@MakeSale, searchList)
//                    rvProducts.adapter = adapter
//                    adapter.onItemClick = {
//                        getProductVariations(it.id)
//                    }
//                }
//                return true
//            }
//
//        })
//
//        if (allProducts.size == 0) {
//            getProducts()
//        }
    }
//
//    private fun getContactsData() {
//        if (isNetworkConnected(this) && allContacts.isEmpty()) {
//            generalViewModel.getUpdatedContacts()
//        }
//        generalViewModel.getLocalContacts().observe(this, androidx.lifecycle.Observer { contacts ->
//            allContacts.clear()
//            allContacts.addAll(contacts)
//            for (i in contacts) {
//                if (i.name == "Walk-In Customer") {
//                    walkinid = i.id.toString()
//
//                }
//            }
//        })
//    }
//
//    private fun addCustomerToBill() {
//        val contactNames: MutableList<String> = arrayListOf()
//        generalViewModel.getLocalContacts().observe(this, androidx.lifecycle.Observer { contacts ->
//            allContacts.clear()
//            allContacts.addAll(contacts)
//            if (contacts.isNotEmpty()) {
//                contactNames.clear()
//                for (i in contacts) {
//                    contactNames.add(i.name)
//                }
//                val dialog = Dialog(this)
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//                dialog.setCancelable(true)
//                dialog.setContentView(R.layout.contact_dialog)
//                val add = dialog.findViewById<TextView>(R.id.add_new_customer)
//                PushDownAnim.setPushDownAnimTo(add)
//                    .setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
//                        val intent = Intent(this, AddNewContact::class.java)
//                        startActivity(intent)
//                    }
//                val spinner = dialog.findViewById<SearchableSpinner>(R.id.customers)
//                val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
//                    this,
//                    android.R.layout.simple_spinner_item,
//                    contactNames as List<CharSequence>
//                )
//                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                spinner.adapter = adapter
//                spinner.setTitle("Select Customer")
//                spinner.onItemSelectedListener =
//                    object : AdapterView.OnItemSelectedListener {
//                        override fun onNothingSelected(p0: AdapterView<*>?) {
//                        }
//
//                        override fun onItemSelected(
//                            p0: AdapterView<*>?,
//                            p1: View?,
//                            p2: Int,
//                            p3: Long
//                        ) {
//                            val selectedItem = p0!!.getItemAtPosition(p2).toString()
//                            for (i in contacts) {
//                                if (i.name == selectedItem) {
//                                    cust_id = i.id.toString()
//                                }
//                            }
//                        }
//                    }
//                val close = dialog.findViewById<TextView>(R.id.close_contact)
//                PushDownAnim.setPushDownAnimTo(close)
//                    .setScale(PushDownAnim.MODE_STATIC_DP, 8F).setOnClickListener {
//                        dialog.dismiss()
//                    }
//                dialog.show()
//                val window = dialog.window
//                window!!.setLayout(
//                    ViewGroup.LayoutParams.MATCH_PARENT,
//                    ViewGroup.LayoutParams.WRAP_CONTENT
//                )
//
//            }
//        })
//
//
//    }
//
//    private fun openDialog() {
////        CartDialog.display(activity!!.supportFragmentManager)
//        if (cust_id == null) {
////            addCustomerToBill()
//            val intent = Intent(this, Cart::class.java)
//            intent.putExtra("cid", walkinid)
//            startActivity(intent)
//        } else {
//            val intent = Intent(this, Cart::class.java)
//            intent.putExtra("cid", cust_id)
//            startActivity(intent)
//        }
//    }
//
//    private fun getCategories() {
//        val categoryNames: MutableList<String> = arrayListOf()
//        GlobalScope.launch { getShifts() }
//        if (isNetworkConnected(this) && allTickets.isEmpty()) {
//            generalViewModel.getUpdatedCategories()
//        }
//        generalViewModel.getLocalCategories()
//            .observe(this, androidx.lifecycle.Observer { categories ->
//                if (categories.isNotEmpty()) {
//                    categoryNames.clear()
//                    categoryNames.add("All Categories")
//                    for (i in categories) {
//                        categoryNames.add(i.name)
//                    }
//                    val adapter: ArrayAdapter<CharSequence> = ArrayAdapter(
//                        this,
//                        android.R.layout.simple_spinner_item,
//                        categoryNames as List<CharSequence>
//                    )
//                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
//                    categories_spinner.adapter = adapter
//                    categories_spinner.setTitle("Select Category")
//                    categories_spinner.onItemSelectedListener =
//                        object : AdapterView.OnItemSelectedListener {
//                            override fun onNothingSelected(p0: AdapterView<*>?) {
//                            }
//
//                            override fun onItemSelected(
//                                p0: AdapterView<*>?,
//                                p1: View?,
//                                p2: Int,
//                                p3: Long
//                            ) {
//                                val selectedItem = p0!!.getItemAtPosition(p2).toString()
//                                if (selectedItem == "All Categories") {
//                                    getProducts()
//                                }
//                                for (i in categories) {
//                                    if (i.name == selectedItem) {
//                                        getProduct(i.id)
//                                    }
//                                }
//                            }
//                        }
//                    AwesomeNoticeDialog(this)
//                        .setTitle("Error")
//                        .setMessage("No Categories found. Please add categories to continue")
//                        .setColoredCircle(R.color.dialogNoticeBackgroundColor)
//                        .setDialogIconAndColor(
//                            R.drawable.ic_notice,
//                            R.color.white
//                        )
//                        .setCancelable(true)
//                        .setButtonText(getString(R.string.dialog_ok_button))
//                        .setButtonBackgroundColor(R.color.dialogNoticeBackgroundColor)
//                        .setButtonText(getString(R.string.dialog_ok_button))
//                        .setNoticeButtonClick {
//                            // click
//                        }
//                        .show()
//                }
//
//
//            })
//
//    }
//
//    private suspend fun getShifts() {
//        try {
//            val response = ApiClient.webService.getShifts(getToken())
//            if (response.isSuccessful) {
//                allShifts.clear()
//                for (i in response.body()!!.shifts) {
//                    if (i.status == "open") {
//                        allShifts.add(i)
//                    }
//                }
//            }
//        } catch (e: IOException) {
//
//        }
//    }
//
//
//    fun View.hideKeyboard() {
//        val inputManager =
//            context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
//        inputManager.hideSoftInputFromWindow(windowToken, 0)
//    }
//
//    private fun getProduct(id: Int) {
//        searchList.clear()
//        for (i in allProducts) {
//            if (i.product.category_id == id) {
//                searchList.add(i)
//            }
//        }
//        productsAdapter = ProductsAdapter(this, searchList)
//        rv_products.adapter = productsAdapter
//        productsAdapter!!.onItemClick = {
//            getProductVariations(it.id)
//        }
//
//
//    }
//
//    private fun getProducts() {
//        if (isNetworkConnected(this) && allTickets.isEmpty()) {
//            generalViewModel.getUpdatedProducts()
//        }
////        if (activity != null) {
//        generalViewModel.getLocalVariations()
//            .observe(this, androidx.lifecycle.Observer { variations ->
//                allProducts.clear()
//                searchList.clear()
//                searchList.addAll(variations)
//                allProducts.addAll(variations)
//                val adapter = ProductsAdapter(this, variations)
//                rvProducts.adapter = adapter
//                adapter.onItemClick = {
//                    getProductVariations(it.id)
//                }
//            })
//
//
//    }
//
//    private fun getProductVariations(id: Int) {
//        val productVariation: MutableList<Variations> = arrayListOf()
//        productVariation.clear()
//        for (p in allProducts) {
//            if (p.id == id) {
//                val ticket = Ticket(
//                    id = p.id,
//                    name = p.name,
//                    product_id = p.product_id,
//                    default_sell_price = p.default_sell_price,
//                    default_purchase_price = p.default_purchase_price,
//                    dpp_inc_tax = p.dpp_inc_tax,
//                    sell_price_inc_tax = p.sell_price_inc_tax,
//                    product = p.product,
//                    sell_price = p.default_sell_price.toDouble(),
//                    quantity = 1
//                )
//                generalViewModel.roomItems(ticket)
//                showTicketItems()
//
////                    getSerialDialog()
//            }
//        }
//
//    }
//
//    private fun showTicketItems() {
//        var sum: Double
//        generalViewModel.getItems().observe(this, androidx.lifecycle.Observer { tickets ->
//            allTickets.clear()
//            allTickets.addAll(tickets)
//            if (tickets.isNotEmpty()) {
//                cartManenos.visibility = View.VISIBLE
//                countTxt.text = allTickets.size.toString()
//            } else {
//                cartManenos.visibility = View.GONE
//            }
//            sum = 0.0
//            for (i in tickets) {
//                sum += i.sell_price
//            }
//            totalTxt.text = sum.toString()
//        })
//
//    }
//
//    override fun onSupportNavigateUp(): Boolean {
//        onBackPressed()
//        return true
//    }
}
