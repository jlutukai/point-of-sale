package com.example.pointofsale.activities

import android.app.Activity
import android.os.Bundle
import com.example.pointofsale.utils.showToast
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView

class BarcodeScanner : Activity(), ZXingScannerView.ResultHandler{
    private lateinit var scannerView: ZXingScannerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scannerView = ZXingScannerView(this)
//        scannerView.flash = true
        scannerView.setAutoFocus(true)
        setContentView(scannerView)
    }

    override fun onResume() {
        super.onResume()
        scannerView.setResultHandler(this)
        scannerView.startCamera()
    }

    override fun onPause() {
        super.onPause()
        scannerView.stopCamera()
    }

    override fun handleResult(rawResult: Result?) {
        if (rawResult != null) {
            showToast(rawResult.text)
            showToast(rawResult.barcodeFormat.toString())
        }
//        scannerView.resumeCameraPreview(this)
    }

}